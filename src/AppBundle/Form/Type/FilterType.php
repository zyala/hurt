<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
    	   ->add('filter.text', 'text', array(
                'label' => '',
                'attr' => array('class' => 'input-sm')))
    	   ->add('search', 'submit')
    	   ->add('reset', 'submit')    	   
        ;		
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_filter';
    }
}
