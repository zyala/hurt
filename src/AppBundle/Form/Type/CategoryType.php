<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('codeMask', 'text', array(
                'label' => 'Indeks',
                'attr' => array('class' => 'input-sm')
            ))
            ->add('brand', 'text', array(
                'label' => 'Marka',
                'attr' => array('class' => 'input-sm')
            ))
            ->add('name', 'text', array(
                'label' => 'Nazwa',
                'attr' => array('class' => 'input-sm')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Category'
        ));
    }
}
