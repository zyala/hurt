<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class ContactMailType extends AbstractType
{
    private $user;
    
    public function __construct($user)
    {
        $this->user = $user;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('section', 'choice', array(
                'label' => ' ',
                'placeholder' => 'Wskaż dział',                
                'attr' => array(
                    'class' => 'input-sm',
                    'input_group' => array(
                        'prepend' => '.icon-envelope-o',
                        'size' => 'small'
                    ),
                ),
                'choices' => $options['data']['office_email'],
                'required' => true
            ))
            
            ->add('name', 'text', array(
                'label' => ' ',                
                'attr' => array(
                    'pattern' => '.{3,}', //minlength
                    'class' => 'input-sm',
                    'input_group' => array(
                        'prepend' => '.icon-user',
                        'size' => 'small'
                        ),
                    'placeholder' => 'Przedstaw się ...'
                 ),                     
                 'data' => ( $this->user=="anon." ? '' : $this->user->getFirstName() . ' ' . $this->user->getLastName() ),
                 'disabled'=> ($this->user=="anon." ? false : true)
            ))
         
            ->add('email', 'email', array(
                'label' => ' ',
                'attr' => array(
                    'class' => 'input-sm',
                    'input_group' => array(
                        'prepend' => '.icon-envelope-o',
                        'size' => 'small'
                    ),
                    'placeholder' => 'Twój adres email ...' 
                ), 
                'data' => ( $this->user=="anon." ? '' : $this->user->getEmail()),
                'disabled'=> ($this->user=="anon." ? false : true)
            ))
            
            ->add('subject', 'text', array(
                'label' => ' ',
                'attr' => array(
                    'class' => 'input-sm',
                    'placeholder' => 'Temat ...'
                ),                
            ))
        
            ->add('message', 'textarea', array(
                'attr' => array(
                    'rows' => 3,
                    'class' => 'input-sm',                
                    'placeholder' => 'Wiadomość ...',
                ),                
                'label' => ' '));
    }
}
