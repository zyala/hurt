<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$current = $options['current'];

    	$permissions = array();
    	$permissions += [ 'ROLE_CLIENT'		=> 'Klient' ];
    	$permissions += [ 'ROLE_STAFF'    	=> 'Pracownik' ];
		if( $current->hasRole( 'ROLE_SUPER_ADMIN' )){
	    	$permissions += [ 'ROLE_SUPER_STAFF'  => 'Superpracownik' ];
   			$permissions += [ 'ROLE_SUPER_ADMIN'  => 'Administrator' ];
		}

    	$builder
    		->add('username', TextType::class, array(
    			'label' => 'Login',
    			'attr' => array('class' => 'input-sm')
    	))
    		->add('firstname', TextType::class, array(
    			'label' => 'Imię',
    			'attr' => array('class' => 'input-sm')
    	))
    		->add('lastname', TextType::class, array(
    			'label' => 'Nazwisko',
    			'attr' => array('class' => 'input-sm')
    	))
    		->add('email', EmailType::class, array(
    			'label' => 'e-Mail',
    			'attr' => array('class' => 'input-sm')
    	))
    		->add('plainPassword', PasswordType::class, array(
    			'label' => 'Hasło',
    			'attr' => array('class' => 'input-sm'),
    			'required' => ($options['data']->getId() ? false : true)
    	))
    		->add('enabled', CheckboxType::class, array(
    			'label' => 'Włączony',
    			'required' => false,
    			'attr' => array(
    					'align_with_widget' => true
    			)
    	))
    		->add('roles', ChoiceType::class, array(
    			'choices' => $permissions,
    			'label' => 'Uprawnienia',
    			'expanded' => false,
    			'multiple' => true,
    	))
    		->add('client', EntityType::class, array(
    			'class' => 'AppBundle:Client',
    			'label' => 'Klient',
    			'property' => 'ShortClient',
    			'empty_data'  => null,
    			'query_builder' => function (EntityRepository $er) {
    				return $er->createQueryBuilder('u')
    				->orderBy('u.shortName', 'ASC');
    			},
    			'required' => false,
    			'attr' => array('class' => 'input-sm')
		));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
			'current' => null        		
        ));
    }
}
