<?php
namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
	
    public function getFilters()
    {
        return array( 
//            new \Twig_SimpleFilter('numberwithzeros', array( $this, 'numberWithZeros'  )),
//            new \Twig_SimpleFilter('printnonzero'   , array( $this, 'printNonZero' )),
            new \Twig_SimpleFilter('price'          , array( $this, 'priceFilter' )),
       		new \Twig_SimpleFilter('quantity'       , array( $this, 'quantityFilter' )),        		
//            new \Twig_SimpleFilter('dayfromnow'     , array( $this, 'dayFromNowFilter' )),
            new \Twig_SimpleFilter('brutto'         , array( $this, 'bruttoFilter' ))
        );
        
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('dateDiff', array( $this, 'dateDiffFunction' )),
            new \Twig_SimpleFunction('strPad', array( $this, 'strPadFunction' ))            
        );
    }

    public function strPadFunction($input, $pad_length, $pad_string = " ", $pad_type = STR_PAD_RIGHT)
    {
        return str_pad($input, $pad_length, $pad_string, $pad_type);
    }
    
    public function dateDiffFunction($datetime1, $datetime2)
    {
        if($datetime1 !== null && $datetime2 !== null){
            return $datetime2->diff($datetime1)->format('%H:%I:%S'); 
        }
        return "";
    }
    
    public function numberWithZeros($number, $width = 0)
    {
        $numberWithZeros = str_pad($number, $width, '0', STR_PAD_LEFT);
        
        return $numberWithZeros;
    }

    public function printNonZero($string)
    {
        $string = ((int) $string == 0 ? html_entity_decode("&nbsp;") : $string);
        
        return $string;
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = $price . " PLN";
        return $price;
    }

    public function quantityFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
    	if(! is_float($number)){
   			$number = floatval($number);
   		}
   		if( $number <> 0){
   			$quantity = number_format($number, $decimals, $decPoint, $thousandsSep);
   			return $quantity;
   		}
   		return '';
    }
    
    public function bruttoFilter($value, $vat)
    {
        $valueBrutto = round($value * (1 + $vat / 100), 2);
        return $valueBrutto;
    }

    public function dayFromNowFilter($date)
    {
        $dateNow = new \DateTime('NOW');
        $days = (int) date_diff($date, $dateNow)->format('%R%a');
        if ($days <= 0) {
            return ('');
        }
        return $days;
    }

    public function getName()
    {
        return 'app_extension';
    }
}
