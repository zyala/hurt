<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * OrderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OrderRepository extends EntityRepository
{
	public function getLastEntity($year)
	{
		$query = $this->createQueryBuilder('a')
		->orderBy('a.year, a.numer', 'DESC')
		->setMaxResults(1)
		->getQuery();
	
		try {
			$order = $query->getSingleResult();
		} catch (\Doctrine\Orm\NoResultException $e) {
			$order = null;
		}
		return $order;
	}
}
