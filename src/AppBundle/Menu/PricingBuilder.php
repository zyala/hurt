<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class PricingBuilder implements ContainerAwareInterface
{
	use ContainerAwareTrait;
	
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root')->setChildrenAttribute('class', 'navbar-nav');
    
        $uri = $this->container->get('request')->getRequestUri();
        if (strpos($uri, '?') != 0) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        $menu->setCurrent($uri);
    
        $menu->addChild('', array(
            'route' => 'about',
        ))->setLinkAttribute('class', 'fa fa-home');
    
        $menu->addChild('Cennik części do samochodów ciężarowych', array(
            'route' => 'pricing_truckpart_index'
        ));
        $menu->addChild('Cennik odlewów żeliwnych', array(
            'route' => 'pricing_ironpart_index'
        ));
        return $menu;
    }
}
