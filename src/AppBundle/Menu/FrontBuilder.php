<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FrontBuilder implements ContainerAwareInterface
{
	use ContainerAwareTrait;
	
	public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root')->setChildrenAttribute('class', 'navbar-nav');
        
        $uri = $this->container->get('request')->getRequestUri();
        if (strpos($uri, '?') != 0) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        $menu->setCurrent($uri);
        
        $menu->addChild('', array('route' => 'about'))->setLinkAttribute('class', 'fa fa-home');
                		
        $menu->addChild('Oferta', array(
            'route' => 'offer'
        ));

        $menu->addChild('Referencje', array(
            'route' => 'credentials'
        ));

        $menu->addChild('Certyfikaty', array(
       		'route' => 'certificates'
        ));

        $menu->addChild('Cenniki', array(
       		'route' => 'pricing_truckpart_index'
        ));

        if($this->container->get('security.context')->isGranted(array('ROLE_STAFF', 'ROLE_SUPER_STAFF', 'ROLE_SUPER_ADMIN'))) {
	        $menu->addChild('e-Zamówienia', array(
    		    'route' => 'b2b'
       		));
        }

//        $menu->addChild('e-Sklep', array(
//            'route' => 'b2c'
//       ));

        $menu->addChild('Kontakt', array(
            'route' => 'contact'
        ));

        $menu->addChild('Napisz do Nas', array(
       		'route' => 'contact_mail'
        ));

        $menu->addChild('Mapa dojazdu', array(
        		'route' => 'map'
        ));
        
        return $menu;
    }
}
