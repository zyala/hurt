<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class B2BBuilder implements ContainerAwareInterface
{
	use ContainerAwareTrait;
	
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root')->setChildrenAttribute('class', 'navbar-nav');
    
        $uri = $this->container->get('request')->getRequestUri();
        if (strpos($uri, '?') != 0) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        $menu->setCurrent($uri);
    
        $menu->addChild('', array(
            'route' => 'about'
        ))->setLinkAttribute('class', 'fa fa-home');
    
        $menu->addChild('Zamawianie', array(
            'route' => 'b2b_ordering_index'
        ));

        $menu->addChild('Klienci', array(
        		'route' => 'admin_client_index'
        ));
        
        $menu->addChild('Oferty', array(
        		'route' => 'staff_offer_index'
        ));
        
//        if($this->container->get('security.context')->isGranted(array('ROLE_SUPER_STAFF', 'ROLE_SUPER_ADMIN'))) {        
//            $menu->addChild('Pracownicy', array(
//           'route' => 'staff_user_index'
//            ));        
//        }
        return $menu;
    }    
}
