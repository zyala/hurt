<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class AdminBuilder implements ContainerAwareInterface
{
	use ContainerAwareTrait;
	
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root')->setChildrenAttribute('class', 'navbar-nav');
    
        $uri = $this->container->get('request')->getRequestUri();
        if (strpos($uri, '?') != 0) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        $menu->setCurrent($uri);
    
        $menu->addChild('', array(
            'route' => 'admin_home'
        ))->setLinkAttribute('class', 'fa fa-home');
    
        $menu->addChild('Logowania', array(
            'route' => 'admin_user_activity_index'
        ));
    
        $menu->addChild('Import', array(
            'route' => 'admin_import_state_index'
        ));

        $menu->addChild('e-Maile', array(
            'route' => 'admin_email_activity_index'
        ));

        $menu->addChild('Kategorie', array(
            'route' => 'admin_category_index'
        ));
        
        $menu->addChild('Klienci', array(
       		'route' => 'admin_client_index'
        ));
                
        $menu->addChild('Administratorzy', array(
            'route' => 'admin_user_index'
        ));
        return $menu;
    }
}
