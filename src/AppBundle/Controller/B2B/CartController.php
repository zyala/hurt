<?php

namespace AppBundle\Controller\B2B;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Product;
use AppBundle\Helper\Helper;
use AppBundle\Cart\Cart;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;

/**
 * Product controller.
 *
 * @Route("/b2b/cart")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class CartController extends Controller
{
    /**
     * Lists all Product entities.
     *
     * @Route("/", name="b2b_cart_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $cart = $request->getSession()->get('b2b_cart', new Cart("b2b_cart"));
        
        list($sort, $filter) = Helper::doSortAndFilter($request, 'b2b_cart_index', 'code', 'asc');
        
        $em = $this->getDoctrine()->getManager();
         
        $query = $em->getRepository('AppBundle:Product')->getQueryCart($sort, $filter, $cart->getItemsId());
         
        $paginator = $this->get('knp_paginator');
         
        $pager = $paginator->paginate($query, $request->query->get('page', 1),
        		$this->getParameter('max_lines_perpage'));

        $cart = $request->getSession()->get('b2b_cart', new Cart("b2b_cart"));
        
        $request->getSession()->set('page_b2b_cart_index', $pager->getCurrentPageNumber());
                
        return $this->render('b2b/cart/index.html.twig', array(        
        		'products' => $pager,
        		'filter' => $filter,
        		'sort' => $sort,
        		'cart_name' => 'b2b'        		
        	));
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     * @Route("/{id}/edit", name="b2b_cart_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Product $product)
    {
    	$cart = $request->getSession()->get('b2b_cart', new Cart("b2b_cart"));
    	 
    	$deleteForm = $this->createDeleteForm($product);
    	$editForm = $this->createForm('AppBundle\Form\Type\CartProductType', array(
    			'cart_product_quantity' => str_replace('.', ',', $cart->getCartProductQuantity($product->getId())),
    			'product' => $product,
    	));
    	$editForm->handleRequest($request);
    	
    	$cart_product_quantity = str_replace(',', '.', $request->request->get('cart_product')['cart_product_quantity']);
    	 
    	if ($editForm->isSubmitted() && $editForm->isValid()) {
    		if ($cart->changeCartProductQuantity($product->getId(), $cart_product_quantity)) {
    			 
    			$request->getSession()->set('b2b_cart', $cart);
    			 
    			$flash = $this->get('braincrafted_bootstrap.flash');
    			$flash->success('Ilość towaru ' . $product->getCodeTransform() . ' w koszyku została zmieniona');    			
    	
    			return $this->redirectToRoute('b2b_cart_index', array(
    					'page' => $request->getSession()->get('page_b2b_cart_index', 1)));
    		}
    	}
    	return $this->render('b2b/cart/edit.html.twig', array(
    			'product' => $product,
    			'edit_form' => $editForm->createView(),
    			'delete_form' => $deleteForm->createView(),
    			'cart_name' => 'b2b'
    	));
    }

    /**
     * Deletes a Product entity.
     *
     * @Route("/{id}", name="b2b_cart_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
    	$form = $this->createDeleteForm($product);
    	$form->handleRequest($request);
    
    	if ($form->isSubmitted() && $form->isValid()) {
    		$cart = $request->getSession()->get('b2b_cart', new Cart('b2b_cart'));
    		$cart->changeCartProductQuantity($product->getId(), 0);
    		$request->getSession()->set('b2b_cart', $cart); 
    		
    		$flash = $this->get('braincrafted_bootstrap.flash');
    		$flash->success('Towar ' . $product->getCodeTransform() . ' został usunięty z koszyka');    		
    	}
    	return $this->redirectToRoute('b2b_cart_index', array(
    			'page' => $request->getSession()->get('page_b2b_cart_index', 1)));
    }

    /**
     * Creates a form to delete a Product entity.
     *
     * @param Product $product The Product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('b2b_cart_delete', array('id' => $product->getId())))
    	->setMethod('DELETE')
    	->getForm()
    	;
    }
    
    /**
     * @Route("/b2b/cart/accept", name="b2b_cart_accept")
     */
    public function acceptCartAction(Request $request)
    {
    	// Defaluts
    	$cart = $request->getSession()->get('b2b_cart', new Cart("b2b_cart"));
    	
    	$clientParams = $session->get('b2b_client_params', null);
    
    	$em = $this->getDoctrine()->getManager();
    
    	$today = new \DateTime('now');
    
    	$client = $em->getRepository('AppBundle:Client')->getEntity($clientParams ? $clientParams['client']->getId() : null);
    
    	$lastOrder = $em->getRepository('AppBundle:Order')->getLastEntity($today->format('Y'));
    
    	$cart->getItemIds();
    
    	$order = new Order();
    	$order->setNumer($lastOrder ? $lastOrder->getNumer() + 1 : 1);
    	$order->setDataDk($today);
    	$order->setYear($today->format('Y'));
    	$order->setStatus("NOWE");
    	$order->setIdKlient($client);
    	$order->setKontr($client->getKontr());
    
    	$em->persist($order);
    
    	foreach ($cart->getItemIds() as $id) {
    
    		$product = $em->getRepository('AppBundle:Product')->getEntity($id);
    
    		$cartProductQuantity = $cart->getCartProductQuantity($id);
    
    		$orderItem = new OrderItem();
    		$orderItem->setOrder($order);
    		$orderItem->setProduct($product);
    		$orderItem->setQuantity($cartProductQuantity);
    		$orderItem->setPricePurchase($product->getPurchasePrice());
    		$orderItem->setPrice($product->getOfferPriceNet());
    		$orderItem->setTax($product->getVat());
    		$orderItem->setPercentDiscountOrder($client->getPercentDiscount());
    		$orderItem->setPercentDiscountItem($product->getPercentDiscount() < $client->getPercentDiscount() ? $product->getPercentDiscount() : $client->getPercentDiscount());
    		$orderItem->setState('NOWE');
    
    		$em->persist($orderItem);
    	}
    
    	$em->flush();
    
    	$orderItems = $em->getRepository('AppBundle:OrderItem')->getEntities($order->getId());
    
    	$subject = "Zamówienie " . trim($order->getNumber()) . " z dnia " . $order->getOrderedAt()->format('Y-m-d') . " z godz. " . $order->getOrderedAt()->format('H:i');
    
    	$message = \Swift_Message::newInstance()->setSubject($subject)
	    	->setFrom($this->getUser()->getEmail())
    		->setTo($this->getParameter('office_department'))
    		->setBody($this->renderView('AppBundle:Cart:email.txt.twig', array(
    			'order' => $order,
    			'orderItems' => $orderItems
    		)));

  		$this->get('mailer')->send($message);
    

  		$flash = $this->get('braincrafted_bootstrap.flash');
  		$flash->success('Zamówiony towar został zaakceptowany, utworzono zamówienie nr ' . $order->getNumber());
  		
    	return $this->redirect($this->generateUrl('b2b_ordering_index', array(
    		'id' => $order->getId())));
    }
}
