<?php

namespace AppBundle\Controller\B2B;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Product;
use AppBundle\Form\Type\CartProductType;
use AppBundle\Helper\Helper;
use AppBundle\Cart\Cart;

/**
 * Product controller.
 *
 * @Route("/b2b/ordering")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class OrderingController extends Controller
{
    /**
     * Lists all Product entities.
     *
     * @Route("/", name="b2b_ordering_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        list($sort, $filter) = Helper::doSortAndFilter($request, 'b2b_ordering2_index', 'code', 'asc');
        
        $em = $this->getDoctrine()->getManager();
         
        $query = $em->getRepository('AppBundle:Product')->getQueryB2B($sort, $filter);
         
        $paginator = $this->get('knp_paginator');
         
        $pager = $paginator->paginate($query, $request->query->get('page', 1),
        		$this->getParameter('max_lines_perpage'));
        
        $request->getSession()->set('page_b2b_ordering_index', $pager->getCurrentPageNumber());
        
        return $this->render('b2b/ordering/index.html.twig', array(
        		'products' => $pager,
        		'filter' => $filter,
        		'sort' => $sort,
        		'cart_name' => 'b2b'
        ));        
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     * @Route("/{id}/edit", name="b2b_ordering_edit")
     * @Method({"GET", "POST"})
     */    
    public function editAction(Request $request, Product $product)
    {
    	$cart = $request->getSession()->get('b2b_cart', new Cart("b2b_cart"));
    	
    	$deleteForm = $this->createDeleteForm($product);
    	$editForm = $this->createForm('AppBundle\Form\Type\CartProductType', array(
    			'cart_product_quantity' => str_replace('.', ',', $cart->getCartProductQuantity($product->getId())),
    			'product' => $product,
    	));
    	$editForm->handleRequest($request);

    	$cart_product_quantity = str_replace(',', '.', $request->request->get('cart_product')['cart_product_quantity']);
    	
    	if ($editForm->isSubmitted() && $editForm->isValid()) {
    		if ($cart->changeCartProductQuantity($product->getId(), $cart_product_quantity)) {
    			
    			$request->getSession()->set('b2b_cart', $cart);    
    			
    			$flash = $this->get('braincrafted_bootstrap.flash');
	    		$flash->success('Ilość towaru ' . $product->getCodeTransform() . ' w koszyku została zmieniona');

    			return $this->redirectToRoute('b2b_ordering_index', array(
    					'page' => $request->getSession()->get('page_b2b_ordering_index', 1)));    			
    		}
    	}
    	return $this->render('b2b/ordering/edit.html.twig', array(
    			'product' => $product,
    			'edit_form' => $editForm->createView(),
    			'delete_form' => $deleteForm->createView(),    			
    			'cart_name' => 'b2b'    			
    	));
    }
    
    /**
     * Deletes a Product entity.
     *
     * @Route("/{id}", name="b2b_ordering_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
    	$form = $this->createDeleteForm($product);
    	$form->handleRequest($request);
    
    	if ($form->isSubmitted() && $form->isValid()) {
    		$cart = $request->getSession()->get('b2b_cart', new Cart('b2b_cart'));
    		$cart->changeCartProductQuantity($product->getId(), 0);
    		$request->getSession()->set('b2b_cart', $cart);  

    		$flash = $this->get('braincrafted_bootstrap.flash');
    		$flash->success('Towar ' . $product->getCodeTransform() . ' został usunięty z koszyka');
    	}
    	return $this->redirectToRoute('b2b_ordering_index', array(
    			'page' => $request->getSession()->get('page_b2b_ordering_index', 1)));
    }

    /**
     * Creates a form to delete a Product entity.
     *
     * @param Product $product The Product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('b2b_ordering_delete', array('id' => $product->getId())))
    	->setMethod('DELETE')
    	->getForm()
    	;
    }
}
