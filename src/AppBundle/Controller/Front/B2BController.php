<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class B2BController extends Controller
{
    /**
     * @Route("/b2b", name="b2b")
     */
    public function b2bAction()
    {
    	return $this->render('front/b2b/b2b.html.twig');
    }
}
