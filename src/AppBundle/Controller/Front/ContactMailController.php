<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\EmailActivity;
use AppBundle\Form\Type\ContactMailType;

class ContactMailController extends Controller
{
    /**
     * @Route("/kontakt/email", name="contact_mail")
     */
    public function contactMailAction(Request $request)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        
        $form = $this->createForm(new ContactMailType($user), array(
            'required' => false,
            'office_email' => $this->container->getParameter('email_contact'),
            'action' => $this->generateUrl('contact_mail'),
            'method' => 'POST'
        ));

        $form->add('send', 'submit', array(
            'label' => 'Wyślij wiadomość',
            'attr' => array("class" => "btn btn-sm btn-primary has-spinner spinner"),
        ));        

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                
                $message = \Swift_Message::newInstance()
                ->setSubject($form->get('subject')->getData())
                ->setFrom($form->get('email')->getData())
                ->setTo($form->get("section")->getData())
                ->setBody($this->renderView('front/contactMail/mail.html.twig', array(
                    'ip' => $request->getClientIp(),
                    'name' => $form->get('name')->getData(),
                    'message' => $form->get('message')->getData()
                )));
        
                $status = $this->get('mailer')->send($message);
                
                $em = $this->getDoctrine()->getManager();
                	
                $entity = new EmailActivity();
                	
                $entity->setSender($form->get('email')->getData());
                $entity->setIdentifyName($form->get('name')->getData());                
                $entity->setRecipient($form->get("section")->getData());
                $entity->setSubject($form->get('subject')->getData());
                $entity->setContentText($form->get('message')->getData());
                $entity->setUnsent(! $status);
                if ( $user == "anon." ){
                } else {
                    $entity->setUser($user);
                }
                $em->persist($entity);
                $em->flush();
                	
                $flash = $this->get('braincrafted_bootstrap.flash');
                $flash->success('Twoja wiadomość została wysłana! Dziękujemy!');

                return $this->redirect($this->generateUrl('contact_mail'));
            }
        }        

        return $this->render('front/contactmail/contactMail.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
