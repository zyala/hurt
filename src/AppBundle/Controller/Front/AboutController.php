<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AboutController extends Controller
{
    /**
     * @Route("/ofirmie", name="about")
     */
    public function aboutAction()
    {
    	return $this->render('front/about/about.html.twig');
    }
}
