<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class OfferController extends Controller
{
	/**
	 * @Route("/oferta", name="offer")
	 */
	public function offerAction()
	{
	    return $this->render('front/offer/offer.html.twig');	
	}
}
