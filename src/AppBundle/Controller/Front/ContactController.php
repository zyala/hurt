<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ContactController extends Controller
{
    /**
     * @Route("/kontakt", name="contact")
     */
    public function contactAction()
    {
        return $this->render('front/contact/contact.html.twig');        
    }
}
