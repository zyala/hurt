<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MapController extends Controller
{
    /**
     * @Route("/mapa-dojazdu", name="map")
     */
    public function mapAction()
    {
        return $this->render('front/map/map.html.twig');
    }
}

