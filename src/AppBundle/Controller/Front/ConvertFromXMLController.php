<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use AppBundle\Command\ConvertFromXMLCommand;

class ConvertFromXMLController extends Controller
{
    /**
     * @Route("/1954081619861002")
     */
    public function convertFromXMLAction()
    {
    	$command = new  ConvertFromXMLCommand();
    	$command->setContainer($this->container);

    	$path = $this->get('kernel')->getRootDir() . '/../web/uploads'; 

        $input = new ArrayInput(array('path' => $path));
    	$output = new NullOutput(array());
    	
 		$command->run($input, $output);
        
 		return $this->render('Front/ConvertFromXML/convertFromXML.html.twig');
    }
}
