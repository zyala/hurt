<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CredentialsController extends Controller
{
    /**
     * @Route("/referencje", name="credentials")
     */
    public function credentialsAction(Request $request )
    {
    	$arrayOfImages = glob($this->get('request')->server->get('DOCUMENT_ROOT') . '/images/hurt/credentials/*-small.jpg');    	

        foreach ($arrayOfImages as $key => $image) {
            $arrayOfImages[$key] = basename($image);
        }
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate($arrayOfImages, $request->query->get('page', 1), 5);

        return $this->render('front/credentials/credentials.html.twig', array(
            'pagination' => $pagination
        ));
    }
}

