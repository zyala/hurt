<?php

namespace AppBundle\Controller\Staff;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Helper\Helper;

/**
 * Product controller.
 *
 * @Route("/staff")
 * @Security("has_role('ROLE_SUPER_ADMIN','ROLE_SUPER_STAFF','ROLE_STAFF')")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="staff_product_index")
     * @Method("GET") 
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
         
        // Sorting and Filtering
        list($sort, $filter) = Helper::doSortAndFilter($request, 'staff_product_index', 'code', 'asc');
    
        $em = $this->getDoctrine()->getManager();
    
        $query = $em->getRepository('AppBundle:Product')->getQueryEntitiesForStaff($sort, $filter);
    
        $paginator = $this->get('knp_paginator');
    
        $pager = $paginator->paginate($query, $request->query->get('page', 1), $this->container->getParameter('max_lines_perpage'));
    
        $session->set('uri_staff_product_index', $request->getUri());
        $session->remove('uri_staff_product_index_alternative');
        
        return $this->render('staff/product/index.html.twig', array(
            'products' => $pager,
            'filter' => $filter,
            'sort' => $sort,
        ));
    }

    /**
     * @Route("/zu1", name="staff_product_index_zu_1")
     * @Method("GET")
     */
    public function indexZU1Action(Request $request)
    {
        $session = $request->getSession();
         
        // Sorting and Filtering
        list($sort, $filter) = Helper::doSortAndFilter($request, 'staff_product_index_zu1', 'code', 'asc');
    
        $em = $this->getDoctrine()->getManager();
    
        $products = $em->getRepository('AppBundle:Product')->getQueryEntitiesForStaffZU1($sort, $filter);

        $session->set('staff_product_index_zu_1', $request->getUri());
        $session->remove('uri_staff_product_index_alternative');
    
        return $this->render('staff/product/index-zu1.html.twig', array(
            'products' => $products,
            'filter' => $filter,
            'sort' => $sort,
        ));
    }    
    /**
     * Lists all Product purchase history entities.
     *
     * @Route("/product/{id}/purchase", name="staff_product_index_purchase")
     * @Method("GET")
     */
    
    public function indexPurchaseAction(Request $request, $id)
    {
        list($sort, $filter) = Helper::doSortAndFilter($request, 'staff_product_index_purchase', 'documentedAt', 'desc');
    
        $em = $this->getDoctrine()->getManager();
    
        $product = $em->getRepository('AppBundle:Product')->getEntity($id);
    
        $query = $em->getRepository('AppBundle:InvoiceItem')->getQueryProductPurchase("Z", $id, $sort, $filter);
    
        $paginator = $this->get('knp_paginator');
    
        $pager = $paginator->paginate($query, $request->query->get('page', 1), $this->container->getParameter('max_lines_perpage_with_header'));

        return $this->render('staff/product/index-purchase.html.twig', array(
            'docs' => $pager,
            'product' => $product,
            'filter'	=> $filter,
            'sort'	=> $sort
        ));
    }
    
    /**
     * Lists all Product purchase history entities.
     *
     * @Route("/product/{id}/alternative", name="staff_product_index_alternative")
     * @Method("GET")
     */
    
    public function indexAlternativeAction(Request $request, $id)
    {
        $session = $request->getSession();
        
        list($sort, $filter) = Helper::doSortAndFilter($request, 'staff_product_index_alternative', 'id', 'asc');
    
        $em = $this->getDoctrine()->getManager();
        
        $alternative = $em->getRepository('AppBundle:Alternative')->getEntityProduct($id);

        $query = $em->getRepository('AppBundle:Alternative')->getQueryProductAlternative( $alternative->getId(), $alternative->getCode(), $sort, $filter);
    
        $paginator = $this->get('knp_paginator');
    
        $pager = $paginator->paginate($query, $request->query->get('page', 1), $this->container->getParameter('max_lines_perpage_with_header'));
    
        $session->set('uri_staff_product_index_alternative', $request->getUri());
        
        return $this->render('staff/product/index-alternative.html.twig', array(
            'alternatives' => $pager,
            'alternative' => $alternative,
            'filter'	=> $filter,
            'sort'	=> $sort
        ));
    }    
}
