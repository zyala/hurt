<?php

namespace AppBundle\Controller\Staff;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Offer;
use AppBundle\Helper\Helper;

/**
 * Offer controller.
 *
 * @Route("/staff_offer_index")
 */
class Offer2Controller extends Controller
{
    /**
     * Lists all Offer entities.
     *
     * @Route("/", name="staff_offer_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	// Sorting and Filtering
    	list($sort, $filter) = Helper::doSortAndFilter($request, 'staff_offer_index', 'validFrom', 'asc');
    	
    	$em = $this->getDoctrine()->getManager();
    	
    	$query = $em->getRepository('AppBundle:Offer')->getQueryEntities($sort, $filter);
    	
    	$paginator = $this->get('knp_paginator');
    	
    	$pager = $paginator->paginate($query, $request->query->get('page', 1), $this->getParameter('max_lines_perpage'));
    	 
    	return $this->render('Staff/Offer/index.html.twig', array(
    			'offers' => $pager,
    			'filter' => $filter,
    			'sort' => $sort,
    	));
    }
    
    /**
     * Creates a new Offer entity.
     *
     * @Route("/new", name="staff_offer_index_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $offer = new Offer();
        $form = $this->createForm('AppBundle\Form\OfferType', $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();

            return $this->redirectToRoute('staff_offer_index_show', array('id' => $offer->getId()));
        }

        return $this->render('offer/new.html.twig', array(
            'offer' => $offer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Offer entity.
     *
     * @Route("/{id}", name="staff_offer_index_show")
     * @Method("GET")
     */
    public function showAction(Offer $offer)
    {
        $deleteForm = $this->createDeleteForm($offer);

        return $this->render('Staff/Offer/show.html.twig', array(
            'offer' => $offer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Offer entity.
     *
     * @Route("/{id}/edit", name="staff_offer_index_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Offer $offer)
    {
        $deleteForm = $this->createDeleteForm($offer);
        $editForm = $this->createForm('AppBundle\Form\OfferType', $offer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();

            return $this->redirectToRoute('staff_offer_index_edit', array('id' => $offer->getId()));
        }

        return $this->render('offer/edit.html.twig', array(
            'offer' => $offer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Offer entity.
     *
     * @Route("/{id}", name="staff_offer_index_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Offer $offer)
    {
        $form = $this->createDeleteForm($offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($offer);
            $em->flush();
        }

        return $this->redirectToRoute('staff_offer_index_index');
    }

    /**
     * Creates a form to delete a Offer entity.
     *
     * @param Offer $offer The Offer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Offer $offer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('staff_offer_index_delete', array('id' => $offer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
