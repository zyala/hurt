<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\ImportState;

/**
 * ImportState controller.
 *
 * @Route("/admin/import/state")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * 
 */
class ImportStateController extends Controller
{
    /**
     * Lists all ImportState entities.
     *
     * @Route("/", name="admin_import_state_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$query = $em->getRepository ( 'AppBundle:ImportState' )->getQueryEntities ();
    	
    	$paginator = $this->get ( 'knp_paginator' );
    	
    	$pager = $paginator->paginate ( $query, $request->query->get ( 'page', 1 ), $this->container->getParameter ( 'max_lines_perpage' ) + 2 );
    	
    	return $this->render('admin/importstate/index.html.twig', array(
			'importStates' => $pager
    	));
    }

    /**
     * Finds and displays a ImportState entity.
     *
     * @Route("/{id}", name="admin_import_state_show")
     * @Method("GET")
     */
    public function showAction(ImportState $importState)
    {
        return $this->render('admin/importstate/show.html.twig', array(
            'importState' => $importState
        ));
    }
}
