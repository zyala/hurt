<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Home controller.
 *
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class HomeController extends Controller
{
    /**
     * @Route("/admin", name="admin_home")
     * @Method("GET")
     */
    public function homeAction()
    {
        return $this->render('admin/home/home.html.twig');        
    }

}
