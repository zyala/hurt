<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Helper\Helper;
use AppBundle\Entity\EmailActivity;

/**
 * EmailActivity controller.
 *
 * @Route("/admin/email/activity")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * 
 */
class EmailActivityController extends Controller
{
    /**
     * Lists all EmailActivity entities.
     *
     * @Route("/", name="admin_email_activity_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	list($sort, $filter) = Helper::doSortAndFilter($request, 'admin_email_activity', 'id', 'desc');
    	 
    	$em = $this->getDoctrine()->getManager();
    	
    	$query = $em->getRepository('AppBundle:EmailActivity')->getQuery($sort, $filter);
    	
    	$paginator = $this->get('knp_paginator');
    	
    	$pager = $paginator->paginate($query, $this->get('request')->query->get('page', 1), $this->container->getParameter('max_lines_perpage'));
    	
    	return $this->render('admin/emailactivity/index.html.twig', array(
    			'emailActivities' => $pager,
    			'filter' => $filter,
    			'sort' => $sort
    	));
    }

    /**
     * Finds and displays a EmailActivity entity.
     *
     * @Route("/{id}", name="admin_email_activity_show")
     * @Method("GET")
     */
    public function showAction(EmailActivity $emailActivity)
    {
        $deleteForm = $this->createDeleteForm($emailActivity);

        return $this->render('admin/emailactivity/show.html.twig', array(
            'emailActivity' => $emailActivity,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * Deletes a EmailActivity entity.
     *
     * @Route("/{id}", name="admin_email_activity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, EmailActivity $emailActivity)
    {
        $form = $this->createDeleteForm($emailActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $emailActivityCopy = clone ($emailActivity);
            try {
            	$em->remove($emailActivity);
            	$em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
            
            	if ($e->getCode() == 0) {
            		if ($e->getPrevious()->getCode() == 23000) {
            			$flash = $this->get('braincrafted_bootstrap.flash');
            			$flash->error('List nr "' . $emailActivity->getId() . '" nie może być usunięty');
            
            			return $this->redirectToRoute('admin_email_activity_show', array('id' => $emailActivity->getId()));
            		} else {
            			throw $e;
            		}
            	} else {
            		throw $e;
            	}
            }
            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('List nr ' . $emailActivityCopy->getId() . ' został usunięty');
        }
        return $this->redirectToRoute('admin_email_activity_index');
    }
    
    /**
     * Creates a form to delete a EmailActivity entity.
     *
     * @param EmailActivity $emailActivity The EmailActivity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EmailActivity $emailActivity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_email_activity_delete', array('id' => $emailActivity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
