<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Helper\Helper;
use AppBundle\Entity\UserActivity;

/**
 * UserActivity controller.
 *
 * @Route("/admin/user/activity")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class UserActivityController extends Controller
{
    /**
     * Lists all UserActivity entities.
     *
     * @Route("/", name="admin_user_activity_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	// Sorting and Filtering
    	list($sort, $filter) = Helper::doSortAndFilter($request, 'admin_user_activity', 'id', 'desc');
    	
    	$em = $this->getDoctrine()->getManager();
    	
    	$query = $em->getRepository('AppBundle:UserActivity')->getQuery($sort, $filter);
    	
    	$paginator = $this->get('knp_paginator');
    	
    	$pager = $paginator->paginate($query, $request->query->get('page', 1), $this->container->getParameter('max_lines_perpage'));

    	return $this->render('admin/useractivity/index.html.twig', array(
    			'userActivities' => $pager,
    			'filter' => $filter,
    			'sort' => $sort
    	));
    }

    /**
     * Finds and displays a UserActivity entity.
     *
     * @Route("/{id}", name="admin_user_activity_show")
     * @Method("GET")
     */
    public function showAction(UserActivity $userActivity)
    {
        $deleteForm = $this->createDeleteForm($userActivity);

        return $this->render('admin/useractivity/show.html.twig', array(
            'userActivity' => $userActivity,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * Deletes a UserActivity entity.
     *
     * @Route("/{id}", name="admin_user_activity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, UserActivity $userActivity)
    {
        $form = $this->createDeleteForm($userActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $userActivityCopy = clone ($userActivity);            
            try {
            	$em->remove($userActivity);
            	$em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
            	if ($e->getCode() == 0)
            	{
            		if ($e->getPrevious()->getCode() == 23000) {
            			$flash = $this->get('braincrafted_bootstrap.flash');
            			$flash->error('Logowanie nr "' . $userActivity->getId() . '" nie może być usunięte!');
            			
            			return $this->redirectToRoute('admin_user_activity_show', array('id' => $userActivity->getId()));            
            		}
            		else{
            			throw $e;
            		}
            	}
            	else{
            		throw $e;
            	}
            }
            
            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Logowanie nr ' . $userActivityCopy->getId() . ' zostało usunięte!');
        }
        return $this->redirectToRoute('admin_user_activity_index');
    }

    /**
     * Creates a form to delete a UserActivity entity.
     *
     * @param UserActivity $userActivity The UserActivity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UserActivity $userActivity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_user_activity_delete', array('id' => $userActivity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
