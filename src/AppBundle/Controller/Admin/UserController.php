<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Helper\Helper;
use AppBundle\Entity\User;

/**
 * User controller.
 *
 * @Route("/admin/user")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_user_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	list($sort, $filter) = Helper::doSortAndFilter($request, 'admin_user_index', 'username', 'asc');
    	 
    	$em = $this->getDoctrine()->getManager();

   		$query = $em->getRepository('AppBundle:User')->getQueryAll($sort, $filter);
    	
    	$paginator = $this->get('knp_paginator');
    	
    	$pager = $paginator->paginate($query, $request->query->get('page', 1),
    			$this->getParameter('max_lines_perpage'));
   	 
    	return $this->render('admin/user/index.html.twig', array(
    			'users' => $pager,
    			'filter' => $filter,
    			'sort' => $sort
    	));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="admin_user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    	$em = $this->container->get('fos_user.user_manager');
    	 
    	$user = $em->createUser();
    	
    	$form = $this->createForm('AppBundle\Form\Type\UserType', $user, array('current' => $this->getUser()));
    	$form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	if (Helper::emptyString($form->getData()->getPassword())) {
        		$user->setPlainPassword($form->getData()
        				->getPlainPassword());
        	}

        	$em->updateUser($user, true);
        	
        	$flash = $this->get('braincrafted_bootstrap.flash');
        	$flash->success('Użytkownik ' . $user->getFullnameWith() . ' został dodany');

            return $this->redirectToRoute('admin_user_show', array('id' => $user->getId()));
        }

        return $this->render('admin/user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="admin_user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('admin/user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\Type\UserType', $user, array('current' => $this->getUser()) );
        
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
        	if ( ! Helper::emptyString($editForm->getData()->getPlainPassword()) ) {
        		$user->setPlainPassword($editForm->getData()->getPlainPassword());
        	}
        	 
        	$user->setPlainPassword($editForm->getData()->getPlainPassword());
        	 
        	$em = $this->container->get('fos_user.user_manager');
        	$em->updateUser($user, true);
        
        	$flash = $this->get('braincrafted_bootstrap.flash');
        	$flash->success('Użytkownik ' . $user->getFullName() . ' został zmieniony');
        	 
        	return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('admin/user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="admin_user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	
        	$em = $this->get('fos_user.user_manager');
        	
        	if ($user->getId() == $this->get('security.context')->getToken()->getUser()->getId())
        	{
        		$flash = $this->get('braincrafted_bootstrap.flash');
        		$flash->success('Użytkownik ' . $user->getFullnameWith() . ' nie może być usunięty');
        		 
        		return $this->redirectToRoute('admin_user_show', array('id' => $user->getId()));
        	}
        	
        	$userCopy = clone ($user);
        	try {
        		$em->deleteUser($user);
        	} catch (\Doctrine\DBAL\DBALException $e) {
        	
        		if ($e->getCode() == 0) {
        			if ($e->getPrevious()->getCode() == 23000) {
        				$flash = $this->get('braincrafted_bootstrap.flash');
        				$flash->error('Użytkownik ' . $user->getFullnameWith() . ' nie może być usunięty');
        	
        				return $this->redirectToRoute('admin_user_show', array('id' => $user->getId()));
        			} else {
        				throw $e;
        			}
        		} else {
        			throw $e;
        		}
        	}
        	$flash = $this->get('braincrafted_bootstrap.flash');
        	$flash->success('Użytkownik ' . $userCopy->getFullnameWith() . ' został usunięty');
       	}        	
       	return $this->redirectToRoute('admin_user_index');
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
