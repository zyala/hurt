<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Helper\Helper;
use AppBundle\Entity\Category;


/**
 * Category controller.
 *
 * @Route("/admin/category")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="admin_category_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	list($sort, $filter) = Helper::doSortAndFilter($request, 'admin_category_index', 'codeMask', 'asc');
    	
    	$em = $this->getDoctrine()->getManager();
    	 
    	$query = $em->getRepository('AppBundle:Category')->getQuery($sort, $filter);
    	 
    	$paginator = $this->get('knp_paginator');
    	 
    	$pager = $paginator->paginate($query, $request->query->get('page', 1),
    			$this->getParameter('max_lines_perpage'));
    	
    	return $this->render('admin/category/index.html.twig', array(
    			'categories' => $pager,
    			'filter' => $filter,
    			'sort' => $sort
    	));
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/new", name="admin_category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm('AppBundle\Form\Type\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            
            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Kategoria ' . $category->getCodeMask() . ' został dodana');

            return $this->redirectToRoute('admin_category_show', array('id' => $category->getId()));
        }

        return $this->render('admin/category/new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Category entity.
     *
     * @Route("/{id}", name="admin_category_show")
     * @Method("GET")
     */
    public function showAction(Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('admin/category/show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("/{id}/edit", name="admin_category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('AppBundle\Form\Type\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            
            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Kategoria ' . $category->getCodeMask() . ' została zmieniona');            

            return $this->redirectToRoute('admin_category_show', array('id' => $category->getId()));
        }
        return $this->render('admin/category/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}", name="admin_category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $categoryCopy = clone ($category);
            try {
            	$em->remove($category);
            	$em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
            	if ($e->getCode() == 0) {
            		if ($e->getPrevious()->getCode() == 23000) {
            			$flash = $this->get('braincrafted_bootstrap.flash');
            			$flash->error('Kategoria ' . $category->getCodeMask() . ' nie może być usunięta');
            			return $this->redirectToRoute('admin_category_show', array('id' => $category->getId()));
            		} else {
            			throw $e;
            		}
            	} else {
            		throw $e;
            	}
            }
            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Kategoria ' . $categoryCopy->getCodeMask() . ' została usunięta');
        }
        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * Creates a form to delete a Category entity.
     *
     * @param Category $category The Category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(category $category)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('b2b_ordering_delete', array('id' => $category->getId())))
    	->setMethod('DELETE')
    	->getForm()
    	;
    }
    
}
