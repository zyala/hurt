<?php

namespace AppBundle\Controller\Pricing;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Helper\Helper;

/**
 * Product controller.
 *
 * @Route("/cennik")
 */
class IronpartController extends Controller
{

    /**
     * Lists all Iron Part entities.
     *
     * @Route("/odlewy-zeliwne", name="pricing_ironpart_index")
     * @Method("GET")
     */
    public function indexAction(Request $request) {

        $limitProductDate = new \DateTime($this->container->getParameter('limit_product_date'));

        // Sorting and Filtering
        list ( $sort, $filter ) = Helper::doSortAndFilter ( $request, 'pricing_ironpart_index', 'code', 'asc' );

        $em = $this->getDoctrine ()->getManager ();

        $query = $em->getRepository ( 'AppBundle:Product' )->getIronQueryEntities( true, $sort, $filter, 0, $limitProductDate );

        $paginator = $this->get ( 'knp_paginator' );

        $pager = $paginator->paginate ( $query, $request->query->get ( 'page', 1 ), $this->container->getParameter ( 'max_lines_perpage' ) );
        
        $paginationData = $pager->getPaginationData();

        return $this->render('pricing/ironpart/index.html.twig', array(
            'ironParts' => $pager,
            'totalRecords' => $paginationData['totalCount'],            
            'filter' => $filter,
            'sort' => $sort
        ));
    }

    /**
     * Prints all Iron Part entities.
     *
     * @Route("/odlewy-zeliwne/drukuj", name="pricing_ironpart_print")
     * @Method("GET")
     */
    public function printPDFAction(Request $request) {

        $limitProductDate = new \DateTime($this->container->getParameter('limit_product_date'));

        $em = $this->getDoctrine ()->getManager ();

        $firm = $em->getRepository ( 'AppBundle:Firm' )->getFirstEntity ();

        // Sorting and Filtering
        list ( $sort, $filter ) = Helper::doSortAndFilter ( $request, 'pricing_ironpart_index', 'code', 'asc' );

        $ironParts = $em->getRepository ( 'AppBundle:Product' )->getIronQueryEntities(false, $sort, $filter, 0, $limitProductDate);

        $this->printPDF($firm, $ironParts);

        return $this->redirectToRoute('pricing_ironpart_index');
    }
    
    private function printPDF($firm, $ironParts){
        $pdf = $this->container->get ( "white_october.tcpdf" )->create ( 'P', 'mm', 'A4', true, 'UTF-8', false );
        $pdf->SetCreator ( 'TCPDF' );
        
        // Set font
        $pdf->SetFont ( 'dejavusans', '', 12, '', true );
        $pdf->setFontSubsetting ( true );
        
        // set Header
        $pdf->SetHeaderMargin ( 5 );
        
        // set Footer
        $pdf->SetFooterMargin ( 20 );
        $pdf->setFooterFont ( Array ('dejavusans','',6) );
        
        // set default monospaced font
        
        // set margins
        
        // set auto page breaks
        $pdf->SetAutoPageBreak ( TRUE, 10 );
        
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage ();
        
        Helper::printHeaderTable( "Cennik odlewów żeliwnych", $pdf, $firm);
         
        // Data Table
        $pdf->SetFillColor ( 221, 221, 221 );
        
        $fill = 0;
        foreach ( $ironParts as $ironPart ) {
            $pdf->Cell( 22, 6, $ironPart->getCodeTransform (), 0, 0, 'L', $fill );
            $pdf->Cell( 80, 6, mb_substr ( $ironPart->getName (), 0, 80, "UTF-8" ), 0, 0, 'L', $fill );
            $pdf->Cell( 40, 6, mb_substr ( $ironPart->getCatalogNo (), 0, 20, "UTF-8" ), 0, 0, 'L', $fill );
            $pdf->Cell(  6, 6, $ironPart->getUnit()->getShortName(), 0, 0, 'L', $fill );
            $pdf->Cell( 21, 6, number_format($ironPart->getOfferPriceNet(), 2, ",", " ") . " PLN", 0, 0, 'R', $fill);
            $pdf->Cell( 21, 6, number_format($this->container->get("app.twig.extension")->bruttoFilter($ironPart->getOfferPriceNet(), $ironPart->getVat()), 2, ",", " ") . " PLN", 0, 1, 'R', $fill);
            $fill = ! $fill;
            if ($pdf->checkPageBreak ( 15 )) {
                $fill = 0;
                Helper::printHeaderTable("Cennik odlewów żeliwnych", $pdf, $firm);
            }
        }
        return $pdf->Output ( 'cennik-odlewy-zeliwne.pdf', 'I' );
    }
}    
