<?php 
namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

use AppBundle\Entity\UserActivity;
use AppBundle\Helper\Helper;

/**
 * Custom login listener.
 */
class LoginListener implements EventSubscriberInterface
{
    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    
    private $parameter;
    
    
    /**
     * Constructor
     *
     * @param Doctrine        $doctrine
     * @param   $parameter 
     */
    public function __construct(Doctrine $doctrine, $parameter)
    {
        $this->em              = $doctrine->getManager();
        $this->parameter       = $parameter;        
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onSecurityImplicitLogin',
        );
    }
    
    /**
     * Do the magic.
     *
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractivelogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        $ip = $event->getRequest()->getClientIp();
#        if($ip <> "127.0.0.1"){
        
            $api_key = $this->parameter['db-ip-api-key'];
            
            list($city, $country, $province) = Helper::visitorDetails( $ip, $api_key);
    
            $entity = new UserActivity();
    
            $entity->setIp($ip);
            $entity->setUser($user);
            $entity->setCity($city);
            $entity->setCountry($country);
            $entity->setProvince($province);
            $this->em->persist($entity);
            $this->em->flush();
        }
#    }
}
