<?php
namespace AppBundle\Cart;

class Cart
{

    protected $key = "cart";

    protected $items = array();

    public function __construct($key = self::KEY)
    {
        $this->key = $key;
    }

    public function getItemCount()
    {
        if (! $this->items) {
            return (0);
        }
        return (sizeof($this->items));
    }

    public function getItemsId()
    {
        $ids = array();
        if (count($this->items) > 0) {
            foreach ($this->items as $id => $quantity) {
                $ids[] = $id;
            }
        }
        return $ids;
    }

    public function emptyCart()
    {
        $this->items = null;
    }

    public function getCartProductQuantity($id)
    {
        if (count($this->items) > 0) {
            if (array_key_exists($id, $this->items)) {
                return ($this->items[$id]);
            }
        }
        return ' ';
    }

    public function changeCartProductQuantity($id, $quantity)
    {
	    if ($quantity > 0) {
    	    $this->items[$id] = floatval($quantity);
        } elseif ($quantity == 0) {
            $this->removeProductFromCart($id);
        } else {
	        return false;
        }
        return true;
    }

    private function removeProductFromCart($id)
    {
        unset($this->items[$id]);
        if (sizeof($this->items) == 0) {
            $this->items = null;
        }
    }
}
