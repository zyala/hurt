<?php
namespace AppBundle\Helper;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\TCPDF\AppPDF;
use AppBundle\Entity\Firm;

class Helper
{
    static public function doSortAndFilter( Request $request, $controllerName, $defaultSortColumn, $defaultSortDirection )
    {
        $session = $request->getSession();
    
        $column = $session->get('sort_column_' .$controllerName, $defaultSortColumn);
    
        $direction = $session->get('sort_direction_' .$controllerName, $defaultSortDirection);
    
        if ( $request->query->get('sort_column') )
        {
            $column  = $request->query->get('sort_column', $column);
    
            $session->set('sort_column_' .$controllerName, $column);
        }
    
        if ( $request->query->get('sort_direction') )
        {
            $direction  = $request->query->get('sort_direction', $direction);
    
            $session->set('sort_direction_' .$controllerName, $direction);
        }

        $text = $session->get('filter_text_' . $controllerName, '');
    
        if ( ! Helper::emptyString($request->query->get('filter_text')) ){
            $text = $request->query->get('filter_text');
        } else {
            if ( $request->query->get('sort_column') || $request->query->get('sort_direction') ){
            }
            else {
                $text = '';
                $text = $session->get('filter_text_' . $controllerName, $text);
    
            }
        }
    
        if ( ! Helper::emptyString($request->query->get('filter_reset')) ) {
            $text = '';
        }
        
        return array(
        				array(
        				    'column' => $column,
        				    'direction' => $direction),
            array(
                'text' => $text,
                'text_parts' => $text == '' ? null : explode(' ',Helper::convertToSlug($text)))
                //        		    	'text_parts' => $text == '' ? null : explode(' ',$text))
        );
    }
    
    static public function convertToSlug($textOrg)
    {
        $inchars = array(
            'ą', 'Ą', 'ć', 'Ć' , 'ę', 'Ę', 'ł', 'Ł', 'ń', 'Ń', 'ó', 'Ó', 'ś' , 'Ś', 'ż', 'Ż', 'ź', 'Ź',
            '`', '~', '!', '@' , '#', '$', '%', '^', '&', '*', '(', ')', '-' , '_', '=', '+', '{', '[',
            '}', ']', '|', '\\', ':', ';', '<', ',', '>', '.', '?', '/', '\'', '"', "'" );
    
        $outchars = array(
            'a', 'A', 'c', 'C' , 'e', 'E', 'l', 'L', 'n', 'N', 'o', 'O', 's' , 'S', 'z', 'Z', 'z', 'Z',
            '',   '',  '',   '',  '',  '',  '',  '',  '',  '',  '',  '',   '',  '',  '',  '',  '',  '',
            '',   '',  '',   '',  '',  '',  '',  '',  '',  '',  '',  '',   '',  '',  '' );
    
        $text = str_replace($inchars, $outchars, $textOrg);
        $text = strtolower($text);
        if(! Helper::emptyString($text)){
        	   return $text;
        } else {
        	   return $textOrg;
        }
    }
    
    static public function emptyString($string) {
        return (is_null($string) || (is_string($string) && (strlen($string) == 0)));
    }
    
    
    static public function printNonZero($string)
    {
        $string = ( (int) $string == 0 ? html_entity_decode("&nbsp;") : $string);
    
        return $string;
    }
    
    static public function visitorDetails( $ip, $api_key ) {
        $result = json_decode(file_get_contents("http://api.db-ip.com/addrinfo?addr=" . $ip ."&api_key=" . $api_key));
    
        if ($result){
            $result = array( $result->city, $result->country, $result->stateprov);
        } else {
            $result = null;
        }
        return $result <> NULL ? $result : "";
    }	
    static public function printHeaderTable($title, AppPDF $pdf, Firm $firm) {
		/**
		 * This method allows printing text with line breaks.
		 * They can be automatic (as soon as the text reaches the right border of the cell) or explicit (via the \n character). As many cells as necessary are output, one below the other.<br />
		 * Text can be aligned, centered or justified. The cell block can be framed and the background painted.
		 * @param $w (float) Width of cells. If 0, they extend up to the right margin of the page.
		 * @param $h (float) Cell minimum height. The cell extends automatically if needed.
		 * @param $txt (string) String to print
		 * @param $border (mixed) Indicates if borders must be drawn around the cell. The value can be a number:<ul><li>0: no border (default)</li><li>1: frame</li></ul> or a string containing some or all of the following characters (in any order):<ul><li>L: left</li><li>T: top</li><li>R: right</li><li>B: bottom</li></ul> or an array of line styles for each border group - for example: array('LTRB' => array('width' => 2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)))
		 * @param $align (string) Allows to center or align the text. Possible values are:<ul><li>L or empty string: left align</li><li>C: center</li><li>R: right align</li><li>J: justification (default value when $ishtml=false)</li></ul>
		 * @param $fill (boolean) Indicates if the cell background must be painted (true) or transparent (false).
		 * @param $ln (int) Indicates where the current position should go after the call. Possible values are:<ul><li>0: to the right</li><li>1: to the beginning of the next line [DEFAULT]</li><li>2: below</li></ul>
		 * @param $x (float) x position in user units
		 * @param $y (float) y position in user units
		 * @param $reseth (boolean) if true reset the last cell height (default true).
		 * @param $stretch (int) font stretch mode: <ul><li>0 = disabled</li><li>1 = horizontal scaling only if text is larger than cell width</li><li>2 = forced horizontal scaling to fit cell width</li><li>3 = character spacing only if text is larger than cell width</li><li>4 = forced character spacing to fit cell width</li></ul> General font stretching and scaling values will be preserved when possible.
		 * @param $ishtml (boolean) INTERNAL USE ONLY -- set to true if $txt is HTML content (default = false). Never set this parameter to true, use instead writeHTMLCell() or writeHTML() methods.
		 * @param $autopadding (boolean) if true, uses internal padding and automatically adjust it to account for line width.
		 * @param $maxh (float) maximum height. It should be >= $h and less then remaining space to the bottom of the page, or 0 for disable this feature. This feature works only when $ishtml=false.
		 * @param $valign (string) Vertical alignment of text (requires $maxh = $h > 0). Possible values are:<ul><li>T: TOP</li><li>M: middle</li><li>B: bottom</li></ul>. This feature works only when $ishtml=false and the cell must fit in a single page.
		 * @param $fitcell (boolean) if true attempt to fit all the text within the cell by reducing the font size (do not work in HTML mode). $maxh must be greater than 0 and wqual to $h.
		 * @return int Return the number of cells or 1 for html mode.
		 * @public
		 * @since 1.3
		 * @see SetFont(), SetDrawColor(), SetFillColor(), SetTextColor(), SetLineWidth(), Cell(), Write(), SetAutoPageBreak()
		 */
	
		$pdf->SetFont ( 'dejavusans', 'B', 8, '', true );
		$pdf->MultiCell ( 80, 8, $firm->getCompanyName(), '', 'L', 0, 0 );
	
		$pdf->SetFont ( 'dejavusans', '', 5, '', true );
		$pdf->MultiCell ( 0, 0, $firm->getCity () . ", dn. " . date( "d.m.Y" ), '', 'R', 0, 0 );
		$pdf->SetFont ( 'dejavusans', '', 10, '', true );
		$pdf->MultiCell ( 0, 4, '', '', 'R', 0, 1 );
	
		$pdf->SetFont ( 'dejavusans', '', 8, '', true );
		$pdf->MultiCell( 80, 8, trim ( $firm->getAddressFull() ), '', 'L', 0, 0 );
		$pdf->SetFont ( 'dejavusans', '', 8, '', true );
		$pdf->MultiCell ( 0, 4, '', '', 'R', 0, 1 );
	
		if (! Helper::emptyString ( $firm->getPhone() )) {
			$pdf->MultiCell ( 80, 8, "telefon : " .$firm->getPhone(), '', 'L', 0, 0 );
			$pdf->SetFont ( 'dejavusans', '', 8, '', true );
			$pdf->MultiCell ( 0, 4, '', '', 'R', 0, 1 );
		}
	
		if (! Helper::emptyString ( $firm->getEmail())) {
			$pdf->MultiCell ( 80, 8, "e-mail : " .$firm->getEmail(), '', 'L', 0, 0 );
			$pdf->SetFont ( 'dejavusans', '', 8, '', true );
			$pdf->MultiCell ( 0, 4, '', '', 'R', 0, 1 );
		}
	
		if (! Helper::emptyString ( $firm->getWebsite())) {
			$pdf->MultiCell ( 80, 8, "www : " .$firm->getWebsite(), '', 'L', 0, 0);
			$pdf->SetFont ( 'dejavusans', '', 8, '', true );
			$pdf->MultiCell ( 0, 4, '', '', 'R', 0, 1 );
		}
	
		// Title
		$pdf->Cell ( 0, 0, $title, 0, 1, 'C' );
		$pdf->Ln ();
	
		// Header Table
		$pdf->SetFont ( 'dejavusans', '', 8, '', true );
		$pdf->Cell ( 22, 6, "Indeks", 'LTB', 0, 'L' );
		$pdf->Cell ( 80, 6, "Nazwa", 'TB', 0, 'L' );
		$pdf->Cell ( 40, 6, "Nr katalogowy", 'TB', 0, 'L' );
		$pdf->Cell ( 6, 6, "Jm", 'TB', 0, 'L' );
		$pdf->Cell ( 21, 6, "Cena netto", 'TB', 0, 'R' );
		$pdf->Cell ( 21, 6, "Cena brutto", 'TBR', 0, 'R' );
		$pdf->Ln ();
	}
}

