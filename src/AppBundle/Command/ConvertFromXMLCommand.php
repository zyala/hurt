<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Reader\ReaderXML;
use AppBundle\Helper\Helper;
use AppBundle\Entity\Firm;
use AppBundle\Entity\Unit;
use AppBundle\Entity\Producer;
use AppBundle\Entity\Client;
use AppBundle\Entity\Product;
use AppBundle\Entity\Alternative;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceItem;
use AppBundle\Entity\Offer;
use AppBundle\Entity\OfferItem;
use AppBundle\Entity\ClientAccount;
use AppBundle\Entity\ImportState;
use Symfony\Component\Filesystem\Filesystem;

class ConvertFromXMLCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('hurt:convert-xml')
            ->setDescription('Konwersja pliku XML')
            ->addArgument('path', InputArgument::OPTIONAL, 'Jaka ścieżka pliku?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$path = $input->getArgument('path');
    	 
    	if( ! $path){
    		$path = $this->getContainer()->get('kernel')->getRootDir() . "/../web/uploads";
    	}

    	$tableOfProgress = array();
        
        $this->importGMPara($tableOfProgress, $path, $output);
        $this->importGMMag($tableOfProgress, $path, $output);
        $this->importGMJm($tableOfProgress, $path, $output);
        $this->importGMProd($tableOfProgress, $path, $output);
        $this->importGMKontr($tableOfProgress, $path, $output);        
        $this->importDKontr($tableOfProgress, $path, $output);
        $this->importGMIndx($tableOfProgress, $path, $output);
        $this->importDIndx($tableOfProgress, $path, $output);
        $this->importGMIndxZ($tableOfProgress, $path, $output);        
        $this->importGMObrfk($tableOfProgress, $path, $output);
        $this->importGMObr($tableOfProgress, $path, $output);
        $this->importGMTablH($tableOfProgress, $path, $output);
        $this->importGMCenyH($tableOfProgress, $path, $output);
        $this->importFkObr($tableOfProgress, $path, $output);
    }

    protected function importGMPara(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMPARA";
        
    	$fs = new Filesystem();

	   	if ( $fs->exists($path . '/' . $fileName . '.XML')) {
		 
    		$tableOfProgress[$fileName] = 0;
    		
    		$this->changeState( $fileName, $tableOfProgress, true, new \DateTime());

        	$xml = new ReaderXML($fileName);
        
       		$xml->loadXmlFile($path . '/' .$fileName . '.XML');

       		$em = $this->getContainer()->get('doctrine')->getManager();

        	while ($xml->valid()) {
            	$data = $xml->current();
                
            	$firm = $em->getRepository('AppBundle:Firm')->getFirstEntity();

	          	if( ! $firm ){
	          		$firm = new Firm();
	          	}
	          	
      	    	$firm->setYear($data['ROK']);
            	$firm->setDayToPaidDefault((int) $data['DNI_ZWLOKI']);
   	        	$firm->setMaxCredit((float) $data['LIMIT_P']);
       	    	$firm->setMinBalance((float) $data['MIN_ROZ']);
                
           		$em->persist($firm);

           		if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
           			$em->flush();
           			$em->clear(); // Detaches all objects from Doctrine!
           		}
 
       	    	$tableOfProgress[$fileName] += 1;
            
           		$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
           		$xml->next();
       		}
       		$em->flush();
       		$em->clear(); // Detaches all objects from Doctrine!

       		$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
       		
       		$xml->__destruct();
        
       		$fs->remove(array($path . '/' . $fileName .'.XML'));

       		$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
	   	}
    }

   	protected function importGMMag(array &$tableOfProgress, $path, OutputInterface $output)
	{
	    $fileName = "GMMAG";
	    
	    $fs = new Filesystem();
    	 
		if ( $fs->exists($path . '/' . $fileName . '.XML')) {
		    	
		    $tableOfProgress[$fileName] = 0;
		
		    $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
		
		    $xml = new ReaderXML($fileName);
		
		    $xml->loadXmlFile($path . '/' .$fileName . '.XML');
    
			$em = $this->getContainer()->get('doctrine')->getManager();
    
			while ($xml->valid()) {

				$data = $xml->current();
    
				$firm = $em->getRepository('AppBundle:Firm')->getFirstEntity();
    
				if ( ! $firm ){
					$firm = new Firm();
				}
	    		$firm->setCompanyName(trim($data['NAZWA1']) .' '. trim($data['NAZWA2']) .' '.trim($data['NAZWA3']));
				$firm->setKrs($data['REJESTR']);
				$firm->setZipCode($data['KOD']);
				$firm->setVatId($data['NIP']);
				$firm->setCity($data['MIASTO']);
				$firm->setAddress($data['ULICA']);
				$firm->setPostOffice($data['POCZTA']);
				$firm->setPhone((string) $data['TELEFON']);
				$firm->setFax($data['FAX']);
				$firm->setEmail($data['EMAIL']);
				$firm->setWebsite($data['WWW']);
    
				$em->persist($firm);

				if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
				    $em->flush();
				    $em->clear(); // Detaches all objects from Doctrine!
				}
				
				$tableOfProgress[$fileName] += 1;
				
				$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
				$xml->next();
			}
			$em->flush();
			$em->clear(); // Detaches all objects from Doctrine!
				
			$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
				 
			$xml->__destruct();
				
			$fs->remove(array($path . '/' . $fileName .'.XML'));
				
			$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
		}
    }
    
    protected function importGMJm(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMJM";
        
    	$fs = new Filesystem();

    	if ( $fs->exists($path . '/' . $fileName . '.XML')) {
    	    	
    	    $tableOfProgress[$fileName] = 0;
    	
    	    $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
    	
    	    $xml = new ReaderXML($fileName);
    	
    	    $xml->loadXmlFile($path . '/' .$fileName . '.XML');
    	
        	$em = $this->getContainer()->get('doctrine')->getManager();
        
        	while ($xml->valid()) {
            	$data = $xml->current();
            
         	   $unit = $em->getRepository('AppBundle:Unit')->getCodeEntity($data['KOD_JM']);
            
	            if (! $unit) {
    	            $unit = new Unit();
        	        $unit->setCode($data['KOD_JM']);
            	}
            	$unit->setShortName($data['SKR_NAZWY']);
	            $unit->setDecimalUnit((bool) $data['CZY_DZIES']);
    	        $unit->setName($data['NAZWA']);
            
        	    $em->persist($unit);

        	    if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
        	        $em->flush();
        	        $em->clear(); // Detaches all objects from Doctrine!
        	    }
        	    
        	    $tableOfProgress[$fileName] += 1;
        	    
        	    $output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
        	    $xml->next();
            }
        	$em->flush();
        	$em->clear(); // Detaches all objects from Doctrine!
        	    
        	$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
        	    	
        	$xml->__destruct();
        	    
        	$fs->remove(array($path . '/' . $fileName .'.XML'));
        	    
        	$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
       	}        
    }

    protected function importGMProd(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMPROD";
        
        $fs = new Filesystem();
    	
        if ( $fs->exists($path . '/' . $fileName . '.XML')) {
            	
            $tableOfProgress[$fileName] = 0;
        
            $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
        
            $xml = new ReaderXML($fileName);
        
            $xml->loadXmlFile($path . '/' .$fileName . '.XML');

            $em = $this->getContainer()->get('doctrine')->getManager();
        
        	while ($xml->valid()) {
            	$data = $xml->current();
            
            	$producer = $em->getRepository('AppBundle:Producer')->getCodeEntity($data['IDPROD']);
            
            	if (! $producer) {
                	$producer = new Producer();
                	$producer->setCode($data['IDPROD']);                
            	}
            	$producer->setName($data['PRODUCENT']);
            
            	$em->persist($producer);
            	
            	if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
            	    $em->flush();
            	    $em->clear(); // Detaches all objects from Doctrine!
            	}
            	
            	$tableOfProgress[$fileName] += 1;
            	
            	$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
            	$xml->next();
            }
            $em->flush();
            $em->clear(); // Detaches all objects from Doctrine!
            	
            $output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
            		
            $xml->__destruct();
            	
            $fs->remove(array($path . '/' . $fileName .'.XML'));
            	
            $this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}
    }

    protected function importGMKontr(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMKONTR";
        
        $fs = new Filesystem();
    	
        if ( $fs->exists($path . '/' . $fileName . '.XML')) {
            	
            $tableOfProgress[$fileName] = 0;
        
            $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
        
            $xml = new ReaderXML($fileName);
        
            $xml->loadXmlFile($path . '/' .$fileName . '.XML');

            $em = $this->getContainer()->get('doctrine')->getManager();            
        
        	while ($xml->valid()) {
            	$data = $xml->current();

            	$client = $em->getRepository('AppBundle:Client')->getCodeEntity($data["KONTR"]);
            
   	        	if (! $client) {
       	        	$client = new Client();
           	    	$client->setCode($data['KONTR']);
           		}
	           	$client->setCompanyName(trim($data['NAZWA2']) .' '. trim($data['NAZWA1']) .' '.trim($data['NAZWA3']));
    	        $client->setVatId($data['NIP']);
   	    	    $client->setShortName($data['SKR_NAZWY']);
       	    	$client->setZipCode($data['KOD']);
           		$client->setCity($data['MIASTO']);
            	$client->setPostOffice($data['POCZTA']);
   	        	$client->setAddress($data['ULICA']);
       	    	$client->setCountryCode($data['KOD_KRAJ']);
           		$client->setPhone($data['TELEFON']);
            	$client->setFax($data['FAX']);
            	$client->setContact($data['KONTAKT']);
   	        	$client->setDaysToPayment((int) $data['DNI_ZWLOKI']);
       	    	$client->setPaymentDefault($data['PLATNOSC']);
            	$client->setPercentDiscount((int) $data['OPUST']);
   	        	$client->setInactive($data['NIEAKTYWNY']);
    	         
       	    	$em->persist($client);
       	    	
       	    	if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
       	    	    $em->flush();
       	    	    $em->clear(); // Detaches all objects from Doctrine!
       	    	}
       	    	
       	    	$tableOfProgress[$fileName] += 1;
       	    	
       	    	$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
       	    	$xml->next();
       	    }
       	    $em->flush();
       	    $em->clear(); // Detaches all objects from Doctrine!
       	    	
       	    $output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
       	    		
       	    $xml->__destruct();
       	    	
       	    $fs->remove(array($path . '/' . $fileName .'.XML'));
       	    	
       	    $this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}        	
    }
    
    protected function importDKontr(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "DKONTR";
        
        $fs = new Filesystem();

        if ( $fs->exists($path . '/' . $fileName . '.XML')) {
            	
            $tableOfProgress[$fileName] = 0;
        
            $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
        
            $xml = new ReaderXML($fileName);
        
            $xml->loadXmlFile($path . '/' .$fileName . '.XML');
    
    		$em = $this->getContainer()->get('doctrine')->getManager();

    		while ($xml->valid()) {
    			$data = $xml->current();
    
    			$client = $em->getRepository('AppBundle:Client')->getCodeEntity($data["KONTR"]);
    
    			if ( $client) {
    				$em->remove($client);
                    $em->persist($client);                    
    			}

    			if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
    			    $em->flush();
    			    $em->clear(); // Detaches all objects from Doctrine!
    			}
    			
    			$tableOfProgress[$fileName] += 1;
    			
    			$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    			$xml->next();
   			}
   			$em->flush();
   			$em->clear(); // Detaches all objects from Doctrine!
    			
   			$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
   				
   			$xml->__destruct();
    			
   			$fs->remove(array($path . '/' . $fileName .'.XML'));
    			
   			$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}
    }
    

    protected function importGMIndx(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMINDX";
        
        $limitStaffProductDate = $this->getContainer()->getParameter('limit_staff_product_date');

    	$fs = new Filesystem();

    	if ( $fs->exists($path . '/' . $fileName . '.XML')) {
    	    	
    	    $tableOfProgress[$fileName] = 0;
    	
    	    $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
    	
    	    $xml = new ReaderXML($fileName);
    	
    	    $xml->loadXmlFile($path . '/' .$fileName . '.XML');
        
        	$em = $this->getContainer()->get('doctrine')->getManager();
  
        	while ($xml->valid()) {
            	$data = $xml->current();

            	if ($data["DATA_R"] >= $limitStaffProductDate) {

            		$product = $em->getRepository('AppBundle:Product')->getCodeEntity($data["INDEKS"]);
                
                	$unit = $em->getRepository('AppBundle:Unit')->getCodeEntity($data["JM"]);

                	$producer = $em->getRepository('AppBundle:Producer')->getCodeEntity($data["IDPROD"]);
                	if (! $producer) {
                    	$producer = $em->getRepository('AppBundle:Producer')->getNameEntity("-");
                	}
                	
	               	$mainCategory = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($data["INDEKS"], 0, 3, "UTF-8"));
	               	if (! $mainCategory) {
	               		$mainCategory = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($data["INDEKS"], 0, 5, "UTF-8"));
	               		if (! $mainCategory) {
	               			$mainCategory = $em->getRepository('AppBundle:Category')->getMaskEntity("     ");
	               		}	                			
	               	}
                	     	
                	$category = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($data["INDEKS"], 0, 5, "UTF-8"));
                	if (! $category) {
                		$category = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($data["INDEKS"], 0, 3, "UTF-8") . "  ");
                    
	                    if (! $category) {
       	                    $category = $em->getRepository('AppBundle:Category')->getMaskEntity("     ");
                	    }
                	}
                	
                	if (! $product) {
                    	$product = new Product();
                    	$product->setCode($data['INDEKS']); 
                	}
                	$product->setCodeSlug(Helper::convertToSlug($data['INDEKS']));                
                	$product->setUnit($unit);
	                $product->setQuantityReserved((float) $data['STAN_REZ']);
    	            $product->setQuantity((float) (float) $data['STAN']);
        	        $product->setVat((string) $data['VAT']);
            	    if($producer){
            	        $product->setProducer($producer);
                	    $product->setNameProducer(Helper::convertToSlug($product->getProducer()->getName()));
            	    }
                	$product->setCategory($category);
                	$product->setMainCategory($mainCategory);
                	$product->setNameMainCategory(Helper::convertToSlug($product->getmainCategory()->getBrand()));
                	$product->setPackage((int) $data['KOMPLET']);
    	            $product->setName($data['NAZWA']);
        	        $product->setNameSlug(Helper::ConvertToSlug($data['NAZWA']));
	                $product->setCatalogNo((string) $data['RYSUNEK']);
    	            $product->setCatalogNoSlug(Helper::ConvertToSlug($data['RYSUNEK']));
                	$product->setPurchasePrice((float) $data['CENA']);
                	$product->setOfferPriceNet((float) $data['CENA_SPRZ']);
                	$product->setWeight((float) $data['WAGA']);
                	$product->setPercentDiscount((int) $data['OPUST']);
                	$product->setInactive($data['NIEAKTYWNY']);
                	$product->setGtin($data['GTIN']);
                	$product->setJim($data['JIM']);
                	$product->setOperationDate(new \DateTime($data['DATA_R']));

                	$product->setIfAlternative(false);                	
                
	                $em->persist($product);

	                if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
	                    $em->flush();
	                    $em->clear(); // Detaches all objects from Doctrine!
	                }
            	}
	                
	            $tableOfProgress[$fileName] += 1;
	                
	            $output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
	            
	            $xml->next();
	        }
	        $em->flush();
	        $em->clear(); // Detaches all objects from Doctrine!
	                
	        $output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
	                	
	        $xml->__destruct();
	                
	        $fs->remove(array($path . '/' . $fileName .'.XML'));
	                
	        $this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
	                 
    	}
    }

    protected function importDIndx(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "DINDX";
        
    	$fs = new Filesystem();

    	if ( $fs->exists($path . '/' . $fileName . '.XML')) {
    	    	
    	    $tableOfProgress[$fileName] = 0;
    	
    	    $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
    	
    	    $xml = new ReaderXML($fileName);
    	
    	    $xml->loadXmlFile($path . '/' .$fileName . '.XML');

    	    $em = $this->getContainer()->get('doctrine')->getManager();
    
    		while ($xml->valid()) {
    			$data = $xml->current();
    
    			$product = $em->getRepository('AppBundle:Product')->getCodeEntity($data["INDEKS"]);
    			if ( $product) {
    				$em->remove($product);
                    $em->persist($product);                    
    			}
    			
    			if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
    			    $em->flush();
    			    $em->clear(); // Detaches all objects from Doctrine!
    			}
    			 
    			$tableOfProgress[$fileName] += 1;
    			 
    			$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    			 
       			$xml->next();
   			}
    		$em->flush();
    		$em->clear(); // Detaches all objects from Doctrine!
    			 
    		$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    			
    		$xml->__destruct();
    			 
    		$fs->remove(array($path . '/' . $fileName .'.XML'));
    			 
    		$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());    			
    	}
    }

    protected function importGMIndxZ(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMINDXZ";
    
        $fs = new Filesystem();
    
        if ( $fs->exists($path . '/' . $fileName . '.XML')) {
    
            $tableOfProgress[$fileName] = 0;
             
            $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
             
            $xml = new ReaderXML($fileName);
             
            $xml->loadXmlFile($path . '/' .$fileName . '.XML');
    
            $em = $this->getContainer()->get('doctrine')->getManager();
            
            $em->getConnection()->exec($em->getConnection()->getDatabasePlatform()->getTruncateTableSql('alternative'));
    
            while ($xml->valid()) {
                $data = $xml->current();
    
                $product = $em->getRepository('AppBundle:Product')->getCodeEntity($data["INDEKS"]);
    
                if ($product) {
                    
                    $alternative = new Alternative();
                    $alternative->setProduct($product);                    
                    $alternative->setCode($data['ZAMIEN']);
                    
                    $em->persist($alternative);
                    
                    $product->setIfAlternative(true); 
                    
                    $em->persist($product);
    
                    if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                }
                $tableOfProgress[$fileName] += 1;
                 
                $output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
                 
                $xml->next();
            }
            $em->flush();
            $em->clear(); // Detaches all objects from Doctrine!
             
            $output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    
            $xml->__destruct();
             
            $fs->remove(array($path . '/' . $fileName .'.XML'));
             
            $this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    
        }
    }
    
    protected function importGMObrFk(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMOBRFK";
        
        $fs = new Filesystem();

        if ( $fs->exists($path . '/' . $fileName . '.XML')) {
            	
            $tableOfProgress[$fileName] = 0;
        
            $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
        
            $xml = new ReaderXML($fileName);
        
            $xml->loadXmlFile($path . '/' .$fileName . '.XML');

            $em = $this->getContainer()->get('doctrine')->getManager();
            
    	    while ($xml->valid()) {
        	    $data = $xml->current();

        	    $client = $em->getRepository('AppBundle:Client')->getCodeEntity($data["KONTR"]);
             
            	$invoice = $em->getRepository('AppBundle:Invoice')->getCodeEntity($data['TYP'], $client->getId(), $data['DATA'], $data['REJESTR'], $data['DOKUMENT'], $data['NUMER'] . $data['NR_EXT']);

            	if (! $invoice) {
	                $invoice = new Invoice();
    	        }    
            	$invoice->setType($data['TYP']);
            	$invoice->setMovementDirection($data['KOD_RR']);
            	$invoice->setRevenue($data['PRZYCHOD']);
            	$invoice->setRegister($data['REJESTR']);
            	$invoice->setDocument($data['DOKUMENT']);
            	$invoice->setNumber($data['NUMER'] . $data['NR_EXT']);
            	$invoice->setClient($client);
            	$invoice->setPayment($data['RODZAJ']);
            	$invoice->setGross($data['BRUTTO']);
            	$invoice->setCash($data['GOTOWKA']);
            	$invoice->setCashOnDelivery($data['POBRANIE']);
            	$invoice->setBankTransfer($data['PRZELEW']);
            	$invoice->setCompensation($data['KOMPENSATA']);
            	$invoice->setPrepayment($data['PRZEDPLATA']);
            	$invoice->setPercentDiscount($data['OPUST']);
            	$invoice->setDocumentedAt(new \DateTime($data['DATA']));
            	$invoice->setMustPayAt(new \DateTime($data['DATA_TERM']));
            	$invoice->setSoldAt(new \DateTime($data['DATA_SPRZ']));
            	$invoice->setMadeAt(new \DateTime($data['DATA_WYST']));
            	$invoice->setOffer($data['OFERTA']);
            	$invoice->setBillFull($data['PAR_FULL']);
            	$invoice->setIndicatorCompany($data['CZY_DZIAL']);

            	$em->persist($invoice);
            	
            	if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
            	    $em->flush();
            	    $em->clear(); // Detaches all objects from Doctrine!
            	}
            	
            	$tableOfProgress[$fileName] += 1;
            	
            	$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
            	$xml->next();
            }
            $em->flush();
            $em->clear(); // Detaches all objects from Doctrine!
            	
            $output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
            		
            $xml->__destruct();
            	
            $fs->remove(array($path . '/' . $fileName .'.XML'));
            	
            $this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}
    }

    protected function importGMObr(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "GMOBR";
        
        $fs = new Filesystem();
    	
        if ( $fs->exists($path . '/' . $fileName . '.XML')) {
             
            $tableOfProgress[$fileName] = 0;
        
            $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
        
            $xml = new ReaderXML($fileName);
        
            $xml->loadXmlFile($path . '/' .$fileName . '.XML');
        
        	$em = $this->getContainer()->get('doctrine')->getManager();
        	
        	while ($xml->valid()) {
            	$data = $xml->current();
            	 
            	$client = $em->getRepository('AppBundle:Client')->getCodeEntity($data["KONTR"]);
            
            	$product = $em->getRepository('AppBundle:Product')->getCodeEntity($data["INDEKS"]);

            	$invoice = $em->getRepository('AppBundle:Invoice')->getCodeEntity($data['TYP'], $client->getId(), $data['DATA'], $data['REJESTR'], substr($data['DOKUMENT'], 0, 2 ), $data['NUMER'] . $data['NR_EXT']);

            	if ( $invoice) {
            		 
            		$invoiceItem = $em->getRepository('AppBundle:InvoiceItem')->getCodeEntity($invoice->getId(), $product->getId(), $data['DOKUMENT'], $data['POZYCJA']);

       				if ( mb_substr($data['INDEKS'], 0, 1, "UTF-8") != "~" ) {

              			if (! $invoiceItem) {
               				$invoiceItem = new InvoiceItem();
               			}
                
               			$invoiceItem->setInvoice($invoice);
               			$invoiceItem->setType($data['TYP']);
               			$invoiceItem->setDocumentedAt(new \DateTime($data['DATA']));
               			$invoiceItem->setDocument($data['DOKUMENT']);
               			$invoiceItem->setSign($data['DOK_ZNAK']);
               			$invoiceItem->setMovementDirection($data['KOD_RR']);
               			$invoiceItem->setRegister($data['REJESTR']);
               			$invoiceItem->setRevenue($data['PRZYCHOD']);
               			$invoiceItem->setItem($data['POZYCJA']);
               			$invoiceItem->setProduct($product);
               			$invoiceItem->setQuantity($data['ILOSC']);
               			$invoiceItem->setPurchasePrice($data['CENA']);
               			$invoiceItem->setOfferPriceNet($data['CENA_S']);
               			$invoiceItem->setVat($data['VAT']);
               			$invoiceItem->setPercentDiscountItem($data['OPUSTPZ']);
                
                		$em->persist($invoiceItem);
                		
                		if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
                		    $em->flush();
                		    $em->clear(); // Detaches all objects from Doctrine!
                		}
       				}
            	}
     
            	$tableOfProgress[$fileName] += 1;
                		
                $output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
                $xml->next();
        	}
           	$em->flush();
           	$em->clear(); // Detaches all objects from Doctrine!
                		
           	$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
                			
           	$xml->__destruct();
                		
           	$fs->remove(array($path . '/' . $fileName .'.XML'));
                		
           	$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}
    }

    protected function importGMTablH(array &$tableOfProgress, $path, OutputInterface $output)
    {
    	$fileName = "GMTABLH";
    
    	$fs = new Filesystem();
    
    	if ( $fs->exists($path . '/' . $fileName . '.XML')) {
    		 
    		$tableOfProgress[$fileName] = 0;
    
    		$this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
    
    		$xml = new ReaderXML($fileName);
    
    		$xml->loadXmlFile($path . '/' .$fileName . '.XML');
    
    		$em = $this->getContainer()->get('doctrine')->getManager();
    
    		while ($xml->valid()) {
    			$data = $xml->current();
    
    			$client = $em->getRepository('AppBundle:Client')->getCodeEntity($data["KONTR"]);
    			 
    			$offer = $em->getRepository('AppBundle:Offer')->getCodeEntity( $client->getId(), new \DateTime($data['DATA_OD']), $data['NR_KO']);
    			
    			if (! $offer) {
    				$offer = new Offer();
    			}

  				$offer->setNumber($data['NR_KO']);
    			$offer->setClient($client);
    			$offer->setValidFrom(new \DateTime($data['DATA_OD']));    			
    			$offer->setPercentDiscount($data['OPUST']);
    			$offer->setProfitR($data['MARZA_D']);
    			$offer->setProfitW($data['MARZA_H']);
    
    			$em->persist($offer);
    			 
    			if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
    				$em->flush();
    				$em->clear(); // Detaches all objects from Doctrine!
    			}
    			 
    			$tableOfProgress[$fileName] += 1;
    			 
    			$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    			$xml->next();
    		}
    		$em->flush();
    		$em->clear(); // Detaches all objects from Doctrine!
    		 
    		$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    
    		$xml->__destruct();
    		 
    		$fs->remove(array($path . '/' . $fileName .'.XML'));
    		 
    		$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}
    }
    
    protected function importGMCenyH(array &$tableOfProgress, $path, OutputInterface $output)
    {
    	$fileName = "GMCENYH";
    	
    	$fs = new Filesystem();
    	 
    	if ( $fs->exists($path . '/' . $fileName . '.XML')) {
    		 
    		$tableOfProgress[$fileName] = 0;
    
    		$this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
    
    		$xml = new ReaderXML($fileName);
    
    		$xml->loadXmlFile($path . '/' .$fileName . '.XML');
    
    		$em = $this->getContainer()->get('doctrine')->getManager();
    		 
    		while ($xml->valid()) {
    			$data = $xml->current();

    			$client = $em->getRepository('AppBundle:Client')->getCodeEntity($data["KONTR"]);
    
    			$product = $em->getRepository('AppBundle:Product')->getCodeEntity($data["INDEKS"]);

    			$offer = $em->getRepository('AppBundle:Offer')->getCodeEntity( $client->getId(), $data['DATA_OD'], $data['NR_KO']);
    			
    			if ( $offer && $product ) {

    				$offerItem = $em->getRepository('AppBundle:OfferItem')->getCodeEntity($offer->getId(), $product->getId());
    				
    				if ( mb_substr($data['INDEKS'], 0, 1, "UTF-8") != "~" ) {
    					if (! $offerItem ) {
    						$offerItem = new OfferItem();
    					}
    
    					$offerItem->setOffer($offer);
    					$offerItem->setProduct($product);    		
    					$offerItem->setSellingPrice($data['CENA_HURT']);    					
    					$offerItem->setQuantity($data['ILOSC']);
    
    					$em->persist($offerItem);
    
    					if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
    						$em->flush();
    						$em->clear(); // Detaches all objects from Doctrine!
    					}
    				}
    			}
    			 
    			$tableOfProgress[$fileName] += 1;
    
    			$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    			$xml->next();
    		}
    		$em->flush();
    		$em->clear(); // Detaches all objects from Doctrine!
    
    		$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
    		 
    		$xml->__destruct();
    
    		$fs->remove(array($path . '/' . $fileName .'.XML'));
    
    		$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}
    }
    
    protected function importFkObr(array &$tableOfProgress, $path, OutputInterface $output)
    {
        $fileName = "FKOBR";
        
        $fs = new Filesystem();

        if ( $fs->exists($path . '/' . $fileName . '.XML')) {
             
            $tableOfProgress[$fileName] = 0;
        
            $this->changeState( $fileName, $tableOfProgress, true, new \DateTime());
        
            $xml = new ReaderXML($fileName);
        
            $xml->loadXmlFile($path . '/' .$fileName . '.XML');
        
        	$em = $this->getContainer()->get('doctrine')->getManager();
        	
        	$em->getConnection()->exec($em->getConnection()->getDatabasePlatform()->getTruncateTableSql('client_account'));
        	 
        	while ($xml->valid()) {
            	$data = $xml->current();
            
            	$clientCode = $data['KONTR'];
            	if (! $clientCode) {
                	$clientCode = mb_substr($data['KONTO'], 5, 5, "UTF-8");
            	}
            
            	$client = $em->getRepository('AppBundle:Client')->getCodeEntity($clientCode);
            
            	if( $client){
	            	$clientAccount = new ClientAccount();
            		$clientAccount->setAccount($data['KONTO']);
            		$clientAccount->setRegister($data['REJESTR']);
	            	$clientAccount->setOperationType($data['RODZAJ']);
    	        	$clientAccount->setInvoiceType($data['FAKTURA']);
        	    	$clientAccount->setSide($data['STRONA']);
            		$clientAccount->setOrginalValue($data['KWOTA']);
	            	$clientAccount->setValue($data['KWOTA_ROZ']);
    	        	$clientAccount->setOpeningBalance($data['BO']);
        	    	$clientAccount->setNumber($data['NUMER'] . $data['NR_EXT']);
            		$clientAccount->setItem($data['POZYCJA']);
            		$clientAccount->setDescription($data['TRESC']);
	            	$clientAccount->setDocumentedAt(new \DateTime($data['DATA']));
            		$clientAccount->setSoldAt(new \DateTime($data['DATA_SPRZ']));
    		       	$clientAccount->setMadeAt(new \DateTime($data['DATA_WYST']));
            		$clientAccount->setMustPayAt(new \DateTime($data['DATA_TERM']));
            		$clientAccount->setClient($client);
         		
            		$em->persist($clientAccount);

            		if (($tableOfProgress[$fileName] %  $this->getContainer()->getParameter('flush_period')) === 0) {
            		    $em->flush();
            		    $em->clear(); // Detaches all objects from Doctrine!
            		}
            	}

            	$tableOfProgress[$fileName] += 1;
            		
            	$output->write( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
            	$xml->next();
           	}
           	$em->flush();
           	$em->clear(); // Detaches all objects from Doctrine!
            		
           	$output->writeln( str_pad($fileName, 8 ) . " : " . $tableOfProgress[$fileName] . "\r");
            			
           	$xml->__destruct();
            		
           	$fs->remove(array($path . '/' . $fileName .'.XML'));
            		
           	$this->changeState( $fileName, $tableOfProgress, false, new \DateTime());
    	}        
    }
    
    private function changeState($fileName, $tableOfProgress, $start, $date)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
     
        $importState = $em->getRepository('AppBundle:ImportState')->getNameEntity($fileName);

        if( ! $importState ){
            $importState = new ImportState();
        }
    
        $importState->setName($fileName);
        $importState->setQuantity($tableOfProgress[$fileName]);
        if( $start){
            $importState->setStartedAt( $date );
            $importState->setFinishedAt( null );            
        } else {            
            $importState->setFinishedAt( $date );            
        }
        
     
        $em->persist($importState);

        $em->flush();
        $em->clear(); // Detaches all objects from Doctrine!
    }
}
