<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Helper\Helper;

class UpdateDataBaseCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('hurt:update')
            ->setDescription('Aktualizacja bazy');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$tableOfProgress = array();
        
        $this->importGMIndx($tableOfProgress, $output);
    }

    protected function importGMIndx(array &$tableOfProgress, OutputInterface $output)
    {
    	$tableOfProgress["GMINDX"] = 0;
    		
    
    	$em = $this->getContainer()->get('doctrine')->getManager();
    
    	$iterableResult = $em->getRepository ( 'AppBundle:Product' )->getAllQueryEntities()->iterate();
    
    	foreach ($iterableResult as $row) {

    		$product = $row[0];
    		
    		if( $product->getProducer() ){
    			$producer = $em->getRepository('AppBundle:Producer')->getCodeEntity($product->getProducer()->getCode());
    		} else {
    			$producer = $em->getRepository('AppBundle:Producer')->getNameEntity("-");
    		}
    			
			$mainCategory = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($product->getCode(), 0, 3, "UTF-8"));
			if (! $mainCategory) {
				$mainCategory = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($product->getCode(), 0, 5, "UTF-8"));
				if (! $mainCategory) {
					$mainCategory = $em->getRepository('AppBundle:Category')->getMaskEntity("     ");
				}
			}
    				 
   			$category = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($product->getCode(), 0, 5, "UTF-8"));
   			if (! $category) {
   				$category = $em->getRepository('AppBundle:Category')->getMaskEntity(mb_substr($product->getCode(), 0, 3, "UTF-8") . "  ");
    
   				if (! $category) {
					$category = $em->getRepository('AppBundle:Category')->getMaskEntity("     ");
   				}
   			}	
   			
   			

   			$product->setProducer($producer);
   			$product->setNameProducer(Helper::convertToSlug($product->getProducer()->getName()));   			
   			$product->setCategory($category);
   			$product->setMainCategory($mainCategory);
   			$product->setNameMainCategory(Helper::convertToSlug($product->getmainCategory()->getBrand()));   			
   
   			$em->persist($product);
    	
			if (($tableOfProgress["GMINDX"] %  $this->getContainer()->getParameter('flush_period')) === 0) {
				$em->flush();
				$em->clear(); // Detaches all objects from Doctrine!
			}

			$tableOfProgress["GMINDX"] += 1;
   			$output->write("  GMINDX : " . $tableOfProgress["GMINDX"] . "\r");
    	}
   		$em->flush();
    	$em->clear(); // Detaches all objects from Doctrine!
    
    	$output->writeln("  GMINDX : " . $tableOfProgress["GMINDX"] . "\r");
    }
    
}
