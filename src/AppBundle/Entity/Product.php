<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Product
 *
 * @ORM\Table(name="product", 
 * 		options={"collate"="utf8_polish_ci"}, 
 * 		indexes={@ORM\Index(name="product_producer", columns={"producer_id"}), 
 *               @ORM\Index(name="product_category", columns={"category_id"}),
 *               @ORM\Index(name="product_main_category", columns={"main_category_id"}),
 *               @ORM\Index(name="product_name_main_category", columns={"name_main_category"}),
 *               @ORM\Index(name="product_unit", columns={"unit_id"}), 
 *               @ORM\Index(name="product_code", columns={"code"})})
 * @ORM\Entity
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=9, nullable=true)
     */
    private $code;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity_reserved", type="float", precision=10, scale=3, nullable=true)
     */
    private $quantityReserved;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float", precision=10, scale=3, nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="vat", type="string", length=2, nullable=true)
     */
    private $vat;

    /**
     * @var integer
     *
     * @ORM\Column(name="package", type="smallint", nullable=true)
     */
    private $package;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_slug", type="string", length=80, nullable=true)
     */
    private $nameSlug;

    /**
     * @var string
     *
     * @ORM\Column(name="catalog_no", type="string", length=20, nullable=true)
     */
    private $catalogNo;

    /**
     * @var string
     *
     * @ORM\Column(name="catalog_no_slug", type="string", length=20, nullable=true)
     */
    private $catalogNoSlug;

    /**
     * @var float
     *
     * @ORM\Column(name="size", type="float", precision=6, scale=0, nullable=true)
     */
    private $size;
    
    /**
     * @var float
     *
     * @ORM\Column(name="purchase_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $purchasePrice;

    /**
     * @var float
     *
     * @ORM\Column(name="offer_price_net", type="float", precision=10, scale=0, nullable=true)
     */
    private $offerPriceNet;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", precision=10, scale=0, nullable=true)
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="percent_discount", type="smallint", nullable=true)
     */
    private $percentDiscount;

    /**
     * @var string
     *
     * @ORM\Column(name="inactive", type="string", length=1, nullable=true)
     */
    private $inactive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="operation_date", type="date", nullable=true)
     */
    private $operationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="code_slug", type="string", length=9, nullable=true)
     */
    private $codeSlug;

    /**
     * @var \Unit
     *
     * @ORM\ManyToOne(targetEntity="Unit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     * })
     */
    private $unit;

    /**
     * @var \Producer
     *
     * @ORM\ManyToOne(targetEntity="Producer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="producer_id", referencedColumnName="id")
     * })
     */
    private $producer;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name_producer", type="string", length=25, nullable=true)
     */
    private $nameProducer;    

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;
    
    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_category_id", referencedColumnName="id")
     * })
     */
    private $mainCategory;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name_main_category", type="string", length=80, nullable=true)
     */
    private $nameMainCategory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="if_alternative", type="boolean", nullable=true)
     */
    private $ifAlternative;
    
    /**
     * @var string
     *
     * @ORM\Column(name="gtin", type="string", length=14, nullable=true)
     */
    private $gtin;    

    /**
     * @var string
     *
     * @ORM\Column(name="jim", type="string", length=14, nullable=true)
     */
    private $jim;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Get codeTransform
     *
     * @return string
     */
    public function getCodeTransform()
    {
    	$codeTransform = $this->code;
    	return mb_substr($codeTransform, 0, 3, "UTF-8") . '-' . mb_substr($codeTransform, 3, 2, "UTF-8") . '-' . mb_substr($codeTransform, 5, 4, "UTF-8");
    }

    /**
     * Set quantityReserved
     *
     * @param float $quantityReserved
     * @return Product
     */
    public function setQuantityReserved($quantityReserved)
    {
        $this->quantityReserved = $quantityReserved;

        return $this;
    }

    /**
     * Get quantityReserved
     *
     * @return float 
     */
    public function getQuantityReserved()
    {
        return $this->quantityReserved;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return Product
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set vat
     *
     * @param string $vat
     * @return Product
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat
     *
     * @return string 
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set package
     *
     * @param integer $package
     * @return Product
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * Get package
     *
     * @return integer 
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameSlug
     *
     * @param string $nameSlug
     * @return Product
     */
    public function setNameSlug($nameSlug)
    {
        $this->nameSlug = $nameSlug;

        return $this;
    }

    /**
     * Get nameSlug
     *
     * @return string 
     */
    public function getNameSlug()
    {
        return $this->nameSlug;
    }

    /**
     * Set catalogNo
     *
     * @param string $catalogNo
     * @return Product
     */
    public function setCatalogNo($catalogNo)
    {
        $this->catalogNo = $catalogNo;

        return $this;
    }

    /**
     * Get catalogNo
     *
     * @return string 
     */
    public function getCatalogNo()
    {
        return $this->catalogNo;
    }

    /**
     * Set catalogNoSlug
     *
     * @param string $catalogNoSlug
     * @return Product
     */
    public function setCatalogNoSlug($catalogNoSlug)
    {
        $this->catalogNoSlug = $catalogNoSlug;

        return $this;
    }

    /**
     * Get catalogNoSlug
     *
     * @return string 
     */
    public function getCatalogNoSlug()
    {
        return $this->catalogNoSlug;
    }

    /**
     * Set purchasePrice
     *
     * @param float $purchasePrice
     * @return Product
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return float 
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set offerPriceNet
     *
     * @param float $offerPriceNet
     * @return Product
     */
    public function setOfferPriceNet($offerPriceNet)
    {
        $this->offerPriceNet = $offerPriceNet;

        return $this;
    }

    /**
     * Get offerPriceNet
     *
     * @return float 
     */
    public function getOfferPriceNet()
    {
        return $this->offerPriceNet;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return Product
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set percentDiscount
     *
     * @param integer $percentDiscount
     * @return Product
     */
    public function setPercentDiscount($percentDiscount)
    {
        $this->percentDiscount = $percentDiscount;

        return $this;
    }

    /**
     * Get percentDiscount
     *
     * @return integer 
     */
    public function getPercentDiscount()
    {
        return $this->percentDiscount;
    }

    /**
     * Set inactive
     *
     * @param string $inactive
     * @return Product
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;

        return $this;
    }

    /**
     * Get inactive
     *
     * @return string 
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * Set operationDate
     *
     * @param \DateTime $operationDate
     * @return Product
     */
    public function setOperationDate($operationDate)
    {
        $this->operationDate = $operationDate;

        return $this;
    }

    /**
     * Get operationDate
     *
     * @return \DateTime 
     */
    public function getOperationDate()
    {
        return $this->operationDate;
    }

    /**
     * Set codeSlug
     *
     * @param string $codeSlug
     * @return Product
     */
    public function setCodeSlug($codeSlug)
    {
        $this->codeSlug = $codeSlug;

        return $this;
    }

    /**
     * Get codeSlug
     *
     * @return string 
     */
    public function getCodeSlug()
    {
        return $this->codeSlug;
    }

    /**
     * Set unit
     *
     * @param \AppBundle\Entity\Unit $unit
     * @return Product
     */
    public function setUnit(\AppBundle\Entity\Unit $unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return \AppBundle\Entity\Unit 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set producer
     *
     * @param \AppBundle\Entity\Producer $producer
     * @return Product
     */
    public function setProducer(\AppBundle\Entity\Producer $producer = null)
    {
        $this->producer = $producer;

        return $this;
    }

    /**
     * Get producer
     *
     * @return \AppBundle\Entity\Producer 
     */
    public function getProducer()
    {
        return $this->producer;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Product
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set mainCategory
     *
     * @param \AppBundle\Entity\Category $mainCategory
     * @return Product
     */
    public function setMainCategory(\AppBundle\Entity\Category $mainCategory = null)
    {
        $this->mainCategory = $mainCategory;

        return $this;
    }

    /**
     * Get mainCategory
     *
     * @return \AppBundle\Entity\Category 
     */
    public function getMainCategory()
    {
        return $this->mainCategory;
    }
    

    /**
     * Set nameMainCategory
     *
     * @param string $nameMainCategory
     * @return Product
     */
    public function setNameMainCategory($nameMainCategory)
    {
        $this->nameMainCategory = $nameMainCategory;

        return $this;
    }

    /**
     * Get nameMainCategory
     *
     * @return string 
     */
    public function getNameMainCategory()
    {
        return $this->nameMainCategory;
    }

    /**
     * Set nameProducer
     *
     * @param string $nameProducer
     * @return Product
     */
    public function setNameProducer($nameProducer)
    {
        $this->nameProducer = $nameProducer;

        return $this;
    }

    /**
     * Get nameProducer
     *
     * @return string 
     */
    public function getNameProducer()
    {
        return $this->nameProducer;
    }

    /**
     * Set ifAlternative
     *
     * @param boolean $ifAlternative
     * @return Product
     */
    public function setIfAlternative($ifAlternative)
    {
        $this->ifAlternative = $ifAlternative;

        return $this;
    }

    /**
     * Get ifAlternative
     *
     * @return boolean 
     */
    public function getIfAlternative()
    {
        return $this->ifAlternative;
    }

    /**
     * Set gtin
     *
     * @param string $gtin
     * @return Product
     */
    public function setGtin($gtin)
    {
        $this->gtin = $gtin;

        return $this;
    }

    /**
     * Get gtin
     *
     * @return string 
     */
    public function getGtin()
    {
        return $this->gtin;
    }

    /**
     * Set jim
     *
     * @param string $jim
     * @return Product
     */
    public function setJim($jim)
    {
        $this->jim = $jim;

        return $this;
    }

    /**
     * Get jim
     *
     * @return string 
     */
    public function getJim()
    {
        return $this->jim;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Product
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Product
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set size
     *
     * @param float $size
     *
     * @return Product
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return float
     */
    public function getSize()
    {
        return $this->size;
    }
}
