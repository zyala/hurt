<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EmailActivity
 *
 * @ORM\Table(name="email_activity",
 *      options={"collate"="utf8_polish_ci"})
 * @ORM\Entity
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmailActivityRepository")
 */
class EmailActivity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sender", type="string", length=80, nullable=true)
     */
    private $sender;
    
    /**
     * @var string
     *
     * @ORM\Column(name="identify_name", type="string", length=80, nullable=true)
     */
    private $identifyName;

    /**
     * @var string
     *
     * @ORM\Column(name="recipient", type="text", nullable=true)
     */
    private $recipient;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="content_text", type="text", nullable=true)
     */
    private $contentText;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="unsent", type="boolean", nullable=true)
     */
    private $unsent;

    /**
     * @var \AppBundle\Entity\\User
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sender
     *
     * @param string $sender
     * @return ActivityEmail
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return string 
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set recipient
     *
     * @param string $recipient
     * @return ActivityEmail
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    
        return $this;
    }

    /**
     * Get recipient
     *
     * @return string 
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return ActivityEmail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set contentText
     *
     * @param string $contentText
     * @return ActivityEmail
     */
    public function setContentText($contentText)
    {
        $this->contentText = $contentText;
    
        return $this;
    }

    /**
     * Get contentText
     *
     * @return string 
     */
    public function getContentText()
    {
        return $this->contentText;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ActivityEmail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ActivityEmail
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set unsent
     *
     * @param boolean $unsent
     * @return ActivityEmail
     */
    public function setUnsent($unsent)
    {
        $this->unsent = $unsent;
    
        return $this;
    }

    /**
     * Get unsent
     *
     * @return boolean 
     */
    public function getUnsent()
    {
        return $this->unsent;
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return ActivityEmail
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set identifyName
     *
     * @param string $identifyName
     * @return EmailActivity
     */
    public function setIdentifyName($identifyName)
    {
        $this->identifyName = $identifyName;

        return $this;
    }

    /**
     * Get identifyName
     *
     * @return string 
     */
    public function getIdentifyName()
    {
        return $this->identifyName;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return EmailActivity
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
