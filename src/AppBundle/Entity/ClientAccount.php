<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ClientAccount
 *
 * @ORM\Table(name="client_account", 
 * 		options={"collate"="utf8_polish_ci"}, 
 * 		indexes={@ORM\Index(name="client", columns={"client_id"})})
 * @ORM\Entity
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientAccountRepository")
 */
class ClientAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="account", type="string", length=10, nullable=true)
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="register", type="string", length=2, nullable=true)
     */
    private $register;

    /**
     * @var string
     *
     * @ORM\Column(name="operation_type", type="string", length=1, nullable=true)
     */
    private $operationType;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_type", type="string", length=1, nullable=true)
     */
    private $invoiceType;

    /**
     * @var string
     *
     * @ORM\Column(name="side", type="string", length=1, nullable=true)
     */
    private $side;

    /**
     * @var float
     *
     * @ORM\Column(name="orginal_value", type="float", precision=10, scale=0, nullable=true)
     */
    private $orginalValue;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float", precision=10, scale=0, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="opening_balance", type="string", length=1, nullable=true)
     */
    private $openingBalance;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=6, nullable=true)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="item", type="string", length=4, nullable=true)
     */
    private $item;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=60, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="documented_at", type="date", nullable=true)
     */
    private $documentedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sold_at", type="date", nullable=true)
     */
    private $soldAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="made_at", type="date", nullable=true)
     */
    private $madeAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="must_pay_at", type="date", nullable=true)
     */
    private $mustPayAt;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientDocument
     *
     * @param string $clientDocument
     * @return ClientAccount
     */
    public function setClientDocument($clientDocument)
    {
        $this->clientDocument = $clientDocument;

        return $this;
    }

    /**
     * Get clientDocument
     *
     * @return string 
     */
    public function getClientDocument()
    {
        return $this->clientDocument;
    }

    /**
     * Set register
     *
     * @param string $register
     * @return ClientAccount
     */
    public function setRegister($register)
    {
        $this->register = $register;

        return $this;
    }

    /**
     * Get register
     *
     * @return string 
     */
    public function getRegister()
    {
        return $this->register;
    }

    /**
     * Set operationType
     *
     * @param string $operationType
     * @return ClientAccount
     */
    public function setOperationType($operationType)
    {
        $this->operationType = $operationType;

        return $this;
    }

    /**
     * Get operationType
     *
     * @return string 
     */
    public function getOperationType()
    {
        return $this->operationType;
    }

    /**
     * Set invoiceType
     *
     * @param string $invoiceType
     * @return ClientAccount
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;

        return $this;
    }

    /**
     * Get invoiceType
     *
     * @return string 
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * Set side
     *
     * @param string $side
     * @return ClientAccount
     */
    public function setSide($side)
    {
        $this->side = $side;

        return $this;
    }

    /**
     * Get side
     *
     * @return string 
     */
    public function getSide()
    {
        return $this->side;
    }

    /**
     * Set orginalValue
     *
     * @param float $orginalValue
     * @return ClientAccount
     */
    public function setOrginalValue($orginalValue)
    {
        $this->orginalValue = $orginalValue;

        return $this;
    }

    /**
     * Get orginalValue
     *
     * @return float 
     */
    public function getOrginalValue()
    {
        return $this->orginalValue;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return ClientAccount
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set openingBalance
     *
     * @param string $openingBalance
     * @return ClientAccount
     */
    public function setOpeningBalance($openingBalance)
    {
        $this->openingBalance = $openingBalance;

        return $this;
    }

    /**
     * Get openingBalance
     *
     * @return string 
     */
    public function getOpeningBalance()
    {
        return $this->openingBalance;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return ClientAccount
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set item
     *
     * @param string $item
     * @return ClientAccount
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return string 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ClientAccount
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set documentedAt
     *
     * @param \DateTime $documentedAt
     * @return ClientAccount
     */
    public function setDocumentedAt($documentedAt)
    {
        $this->documentedAt = $documentedAt;

        return $this;
    }

    /**
     * Get documentedAt
     *
     * @return \DateTime 
     */
    public function getDocumentedAt()
    {
        return $this->documentedAt;
    }

    /**
     * Set soldAt
     *
     * @param \DateTime $soldAt
     * @return ClientAccount
     */
    public function setSoldAt($soldAt)
    {
        $this->soldAt = $soldAt;

        return $this;
    }

    /**
     * Get soldAt
     *
     * @return \DateTime 
     */
    public function getSoldAt()
    {
        return $this->soldAt;
    }

    /**
     * Set madeAt
     *
     * @param \DateTime $madeAt
     * @return ClientAccount
     */
    public function setMadeAt($madeAt)
    {
        $this->madeAt = $madeAt;

        return $this;
    }

    /**
     * Get madeAt
     *
     * @return \DateTime 
     */
    public function getMadeAt()
    {
        return $this->madeAt;
    }

    /**
     * Set mustPayAt
     *
     * @param \DateTime $mustPayAt
     * @return ClientAccount
     */
    public function setMustPayAt($mustPayAt)
    {
        $this->mustPayAt = $mustPayAt;

        return $this;
    }

    /**
     * Get mustPayAt
     *
     * @return \DateTime 
     */
    public function getMustPayAt()
    {
        return $this->mustPayAt;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return ClientAccount
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ClientAccount
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ClientAccount
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set account
     *
     * @param string $account
     * @return ClientAccount
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return ClientAccount
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return ClientAccount
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
