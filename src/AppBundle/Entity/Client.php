<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Client
 *
 * @ORM\Table(name="client", 
 * 		options={"collate"="utf8_polish_ci"}, 
 * 		indexes={@ORM\Index(name="client_code", columns={"code"})})
 * @ORM\Entity
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=5, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=150, nullable=true)
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_id", type="string", length=13, nullable=true)
     */
    private $vatId;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=25, nullable=true)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=5, nullable=true)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=25, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="post_office", type="string", length=25, nullable=true)
     */
    private $postOffice;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=35, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", length=2, nullable=true)
     */
    private $countryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=25, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=25, nullable=true)
     */
    private $contact;

    /**
     * @var integer
     *
     * @ORM\Column(name="days_to_payment", type="smallint", nullable=true)
     */
    private $daysToPayment;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_default", type="string", length=1, nullable=true)
     */
    private $paymentDefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="percent_discount", type="smallint", nullable=true)
     */
    private $percentDiscount;

    /**
     * @var string
     *
     * @ORM\Column(name="inactive", type="string", length=1, nullable=true)
     */
    private $inactive;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Client
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set vatId
     *
     * @param string $vatId
     * @return Client
     */
    public function setVatId($vatId)
    {
        $this->vatId = $vatId;

        return $this;
    }

    /**
     * Get vatId
     *
     * @return string 
     */
    public function getVatId()
    {
        return $this->vatId;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return Client
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }
    
    /**
     * Get shortNameCity
     *
     * @return string
     */
    public function getShortNameCity()
    {
    	return $this->shortName . ", " . $this->city;
    }
    
    
    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return Client
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Client
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postOffice
     *
     * @param string $postOffice
     * @return Client
     */
    public function setPostOffice($postOffice)
    {
        $this->postOffice = $postOffice;

        return $this;
    }

    /**
     * Get postOffice
     *
     * @return string 
     */
    public function getPostOffice()
    {
        return $this->postOffice;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get adres
     *
     * @return string
     */
    public function getAddressFull()
    {
    	$address = '';
    	if (strlen($this->getZipCode()) != 0) {
    		$address .= substr($this->getZipCode(), 0, 2) . '-' . substr($this->getZipCode(), 2, 3) . ' ';
    	}
    	$address .= trim($this->getPostOffice());
    	if (trim($this->getPostOffice()) != trim($this->getCity())) {
    		$address .= ", " . trim($this->getCity());
    	}
    	if (strlen($this->getAddress()) != 0) {
    		if (stripos('0123456789', substr($this->getAddress(), 0, 1))) {
    			$address .= ' ';
    		} else {
    			$address .= ', ';
    		}
    	}
    	$address .= trim($this->getAddress());
    
    	return $address;
    }
    

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return Client
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Client
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Client
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Client
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set daysToPayment
     *
     * @param integer $daysToPayment
     * @return Client
     */
    public function setDaysToPayment($daysToPayment)
    {
        $this->daysToPayment = $daysToPayment;

        return $this;
    }

    /**
     * Get daysToPayment
     *
     * @return integer 
     */
    public function getDaysToPayment()
    {
        return $this->daysToPayment;
    }

    /**
     * Set paymentDefault
     *
     * @param string $paymentDefault
     * @return Client
     */
    public function setPaymentDefault($paymentDefault)
    {
        $this->paymentDefault = $paymentDefault;

        return $this;
    }

    /**
     * Get paymentDefault
     *
     * @return string 
     */
    public function getPaymentDefault()
    {
        return $this->paymentDefault;
    }

    /**
     * Set percentDiscount
     *
     * @param integer $percentDiscount
     * @return Client
     */
    public function setPercentDiscount($percentDiscount)
    {
        $this->percentDiscount = $percentDiscount;

        return $this;
    }

    /**
     * Get percentDiscount
     *
     * @return integer 
     */
    public function getPercentDiscount()
    {
        return $this->percentDiscount;
    }

    /**
     * Set inactive
     *
     * @param string $inactive
     * @return Client
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;

        return $this;
    }

    /**
     * Get inactive
     *
     * @return string 
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Client
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Client
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Client
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Client
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    
    /**
     * Get Short Client
     *
     * @return string
     */
    public function getShortClient()
    {
    	return $this->code . ' ' . $this->shortName . ' ' . $this->countryCode . $this->vatId 
    		. ' ' . $this->getAddressFull(); 
    	
    }    

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Client
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}
