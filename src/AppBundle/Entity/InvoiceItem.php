<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * InvoiceItem
 *
 * @ORM\Table(name="invoice_item", 
 * 		options={"collate"="utf8_polish_ci"}, 
 * 		indexes={@ORM\Index(name="invoice", columns={"invoice_id"}), @ORM\Index(name="product", columns={"product_id"}), @ORM\Index(name="invoice_item", columns={"invoice_id", "product_id", "item"})})
 * @ORM\Entity
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceItemRepository")
 */
class InvoiceItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     *
     * @var string @ORM\Column(name="type", type="string", length=1, nullable=true)
     */
    private $type;    

    /**
     * @var string
     *
     * @ORM\Column(name="document", type="string", length=3, nullable=true)
     */
    private $document;
    
    /**
     *
     * @var \DateTime @ORM\Column(name="documented_at", type="date", nullable=true)
     */
    private $documentedAt;    

    /**
     * @var string
     *
     * @ORM\Column(name="sign", type="string", length=1, nullable=true)
     */
    private $sign;

    /**
     * @var string
     *
     * @ORM\Column(name="movement_direction", type="string", length=3, nullable=true)
     */
    private $movementDirection;

    /**
     * @var string
     *
     * @ORM\Column(name="register", type="string", length=2, nullable=true)
     */
    private $register;

    /**
     * @var string
     *
     * @ORM\Column(name="revenue", type="string", length=1, nullable=true)
     */
    private $revenue;

    /**
     * @var string
     *
     * @ORM\Column(name="item", type="string", length=3, nullable=false)
     */
    private $item;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float", precision=10, scale=0, nullable=true)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="offer_price_net", type="float", precision=10, scale=0, nullable=true)
     */
    private $offerPriceNet;

    /**
     * @var float
     *
     * @ORM\Column(name="purchase_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $purchasePrice;

    /**
     * @var string
     *
     * @ORM\Column(name="vat", type="string", length=2, nullable=true)
     */
    private $vat;

    /**
     * @var float
     *
     * @ORM\Column(name="percent_discount_item", type="float", precision=10, scale=2, nullable=true)
     */
    private $percentDiscountItem;
    
    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Invoice
     *
     * @ORM\ManyToOne(targetEntity="Invoice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     */
    private $invoice;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set document
     *
     * @param string $document
     * @return InvoiceItem
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set sign
     *
     * @param string $sign
     * @return InvoiceItem
     */
    public function setSign($sign)
    {
        $this->sign = $sign;

        return $this;
    }

    /**
     * Get sign
     *
     * @return string 
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * Set movementDirection
     *
     * @param string $movementDirection
     * @return InvoiceItem
     */
    public function setMovementDirection($movementDirection)
    {
        $this->movementDirection = $movementDirection;

        return $this;
    }

    /**
     * Get movementDirection
     *
     * @return string 
     */
    public function getMovementDirection()
    {
        return $this->movementDirection;
    }

    /**
     * Set register
     *
     * @param string $register
     * @return InvoiceItem
     */
    public function setRegister($register)
    {
        $this->register = $register;

        return $this;
    }

    /**
     * Get register
     *
     * @return string 
     */
    public function getRegister()
    {
        return $this->register;
    }

    /**
     * Set revenue
     *
     * @param string $revenue
     * @return InvoiceItem
     */
    public function setRevenue($revenue)
    {
        $this->revenue = $revenue;

        return $this;
    }

    /**
     * Get revenue
     *
     * @return string 
     */
    public function getRevenue()
    {
        return $this->revenue;
    }

    /**
     * Set item
     *
     * @param string $item
     * @return InvoiceItem
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return string 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return InvoiceItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set offerPriceNet
     *
     * @param float $offerPriceNet
     * @return InvoiceItem
     */
    public function setOfferPriceNet($offerPriceNet)
    {
        $this->offerPriceNet = $offerPriceNet;

        return $this;
    }

    /**
     * Get offerPriceNet
     *
     * @return float 
     */
    public function getOfferPriceNet()
    {
        return $this->offerPriceNet;
    }

    /**
     * Set purchasePrice
     *
     * @param float $purchasePrice
     * @return InvoiceItem
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return float 
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set vat
     *
     * @param string $vat
     * @return InvoiceItem
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat
     *
     * @return string 
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set percentDiscountItem
     *
     * @param float $percentDiscountItem
     * @return InvoiceItem
     */
    public function setPercentDiscountItem($percentDiscountItem)
    {
        $this->percentDiscountItem = $percentDiscountItem;

        return $this;
    }

    /**
     * Get percentDiscountItem
     *
     * @return float 
     */
    public function getPercentDiscountItem()
    {
        return $this->percentDiscountItem;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return InvoiceItem
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     * @return InvoiceItem
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\Invoice 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return InvoiceItem
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return InvoiceItem
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set documentedAt
     *
     * @param \DateTime $documentedAt
     * @return InvoiceItem
     */
    public function setDocumentedAt($documentedAt)
    {
        $this->documentedAt = $documentedAt;

        return $this;
    }

    /**
     * Get documentedAt
     *
     * @return \DateTime 
     */
    public function getDocumentedAt()
    {
        return $this->documentedAt;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return InvoiceItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return InvoiceItem
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return InvoiceItem
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
