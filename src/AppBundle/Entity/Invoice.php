<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice", 
 * 		options={"collate"="utf8_polish_ci"}, 
 * 		indexes={@ORM\Index(name="client", columns={"client_id"}), @ORM\Index(name="invoice_number", columns={"type", "documented_at", "register", "document", "number"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceRepository")
 */
class Invoice {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="bigint", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="type", type="string", length=1, nullable=true)
	 */
	private $type;
	
	/**
	 *
	 * @var string @ORM\Column(name="movement_direction", type="string", length=3, nullable=true)
	 */
	private $movementDirection;
	
	/**
	 *
	 * @var string @ORM\Column(name="revenue", type="string", length=1, nullable=true)
	 */
	private $revenue;
	
	/**
	 *
	 * @var string @ORM\Column(name="register", type="string", length=2, nullable=true)
	 */
	private $register;
	
	/**
	 *
	 * @var string @ORM\Column(name="document", type="string", length=3, nullable=true)
	 */
	private $document;
	
	/**
	 *
	 * @var string @ORM\Column(name="number", type="string", length=6, nullable=true)
	 */
	private $number;
	
	/**
	 *
	 * @var string @ORM\Column(name="payment", type="string", length=3, nullable=true)
	 */
	private $payment;
	
	/**
	 *
	 * @var float @ORM\Column(name="gross", type="float", precision=10, scale=0, nullable=true)
	 */
	private $gross;
	
	/**
	 *
	 * @var float @ORM\Column(name="cash", type="float", precision=10, scale=0, nullable=true)
	 */
	private $cash;
	
	/**
	 *
	 * @var float @ORM\Column(name="cash_on_delivery", type="float", precision=10, scale=0, nullable=true)
	 */
	private $cashOnDelivery;
	
	/**
	 *
	 * @var float @ORM\Column(name="bank_transfer", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bankTransfer;
	
	/**
	 *
	 * @var float @ORM\Column(name="compensation", type="float", precision=10, scale=0, nullable=true)
	 */
	private $compensation;
	
	/**
	 *
	 * @var float @ORM\Column(name="prepayment", type="float", precision=10, scale=0, nullable=true)
	 */
	private $prepayment;
	
	/**
	 *
	 * @var float @ORM\Column(name="percent_discount", type="float", precision=10, scale=2, nullable=true)
	 */
	private $percentDiscount;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="documented_at", type="date", nullable=true)
	 */
	private $documentedAt;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="must_pay_at", type="date", nullable=true)
	 */
	private $mustPayAt;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="sold_at", type="date", nullable=true)
	 */
	private $soldAt;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="made_at", type="date", nullable=true)
	 */
	private $madeAt;
	
	/**
	 *
	 * @var string @ORM\Column(name="offer", type="string", length=32, nullable=true)
	 */
	private $offer;
	
	/**
	 *
	 * @var boolean @ORM\Column(name="bill_full", type="boolean", nullable=true)
	 */
	private $billFull;
	
	/**
	 *
	 * @var boolean @ORM\Column(name="indicator_company", type="boolean", nullable=true)
	 */
	private $indicatorCompany;
	
	/**
	 *
	 * @var \Client @ORM\ManyToOne(targetEntity="Client")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="client_id", referencedColumnName="id")
	 *      })
	 */
	private $client;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime", nullable=true)
	 * @Gedmo\Timestampable(on="create")
	 */
	private $createdAt;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 * @Gedmo\Timestampable(on="update")
	 */
	private $updatedAt;

	/**
	 * @var \AppBundle\Entity\User
	 *
	 * @Gedmo\Blameable(on="create")
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
	 * })
	 */
	private $createdBy;
	
	/**
	 * @var \AppBundle\Entity\User
	 *
	 * @Gedmo\Blameable(on="update")
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
	 * })
	 */
	private $updatedBy;	
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set type
	 *
	 * @param string $type        	
	 * @return Invoice
	 */
	public function setType($type) {
		$this->type = $type;
		
		return $this;
	}
	
	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Set movementDirection
	 *
	 * @param string $movementDirection        	
	 * @return Invoice
	 */
	public function setMovementDirection($movementDirection) {
		$this->movementDirection = $movementDirection;
		
		return $this;
	}
	
	/**
	 * Get movementDirection
	 *
	 * @return string
	 */
	public function getMovementDirection() {
		return $this->movementDirection;
	}
	
	/**
	 * Set revenue
	 *
	 * @param string $revenue        	
	 * @return Invoice
	 */
	public function setRevenue($revenue) {
		$this->revenue = $revenue;
		
		return $this;
	}
	
	/**
	 * Get revenue
	 *
	 * @return string
	 */
	public function getRevenue() {
		return $this->revenue;
	}
	
	/**
	 * Set register
	 *
	 * @param string $register        	
	 * @return Invoice
	 */
	public function setRegister($register) {
		$this->register = $register;
		
		return $this;
	}
	
	/**
	 * Get register
	 *
	 * @return string
	 */
	public function getRegister() {
		return $this->register;
	}
	
	/**
	 * Set document
	 *
	 * @param string $document        	
	 * @return Invoice
	 */
	public function setDocument($document) {
		$this->document = $document;
		
		return $this;
	}
	
	/**
	 * Get document
	 *
	 * @return string
	 */
	public function getDocument() {
		return $this->document;
	}
	
	/**
	 * Set number
	 *
	 * @param string $number        	
	 * @return Invoice
	 */
	public function setNumber($number) {
		$this->number = $number;
		
		return $this;
	}
	
	/**
	 * Get number
	 *
	 * @return string
	 */
	public function getNumber() {
		return $this->number;
	}
	
	/**
	 * Set payment
	 *
	 * @param string $payment        	
	 * @return Invoice
	 */
	public function setPayment($payment) {
		$this->payment = $payment;
		
		return $this;
	}
	
	/**
	 * Get payment
	 *
	 * @return string
	 */
	public function getPayment() {
		return $this->payment;
	}
	
	/**
	 * Set gross
	 *
	 * @param float $gross        	
	 * @return Invoice
	 */
	public function setGross($gross) {
		$this->gross = $gross;
		
		return $this;
	}
	
	/**
	 * Get gross
	 *
	 * @return float
	 */
	public function getGross() {
		return $this->gross;
	}
	
	/**
	 * Set cash
	 *
	 * @param float $cash        	
	 * @return Invoice
	 */
	public function setCash($cash) {
		$this->cash = $cash;
		
		return $this;
	}
	
	/**
	 * Get cash
	 *
	 * @return float
	 */
	public function getCash() {
		return $this->cash;
	}
	
	/**
	 * Set cashOnDelivery
	 *
	 * @param float $cashOnDelivery        	
	 * @return Invoice
	 */
	public function setCashOnDelivery($cashOnDelivery) {
		$this->cashOnDelivery = $cashOnDelivery;
		
		return $this;
	}
	
	/**
	 * Get cashOnDelivery
	 *
	 * @return float
	 */
	public function getCashOnDelivery() {
		return $this->cashOnDelivery;
	}
	
	/**
	 * Set bankTransfer
	 *
	 * @param float $bankTransfer        	
	 * @return Invoice
	 */
	public function setBankTransfer($bankTransfer) {
		$this->bankTransfer = $bankTransfer;
		
		return $this;
	}
	
	/**
	 * Get bankTransfer
	 *
	 * @return float
	 */
	public function getBankTransfer() {
		return $this->bankTransfer;
	}
	
	/**
	 * Set compensation
	 *
	 * @param float $compensation        	
	 * @return Invoice
	 */
	public function setCompensation($compensation) {
		$this->compensation = $compensation;
		
		return $this;
	}
	
	/**
	 * Get compensation
	 *
	 * @return float
	 */
	public function getCompensation() {
		return $this->compensation;
	}
	
	/**
	 * Set prepayment
	 *
	 * @param float $prepayment        	
	 * @return Invoice
	 */
	public function setPrepayment($prepayment) {
		$this->prepayment = $prepayment;
		
		return $this;
	}
	
	/**
	 * Get prepayment
	 *
	 * @return float
	 */
	public function getPrepayment() {
		return $this->prepayment;
	}
	
	/**
	 * Set percentDiscount
	 *
	 * @param float $percentDiscount        	
	 * @return Invoice
	 */
	public function setPercentDiscount($percentDiscount) {
		$this->percentDiscount = $percentDiscount;
		
		return $this;
	}
	
	/**
	 * Get percentDiscount
	 *
	 * @return float
	 */
	public function getPercentDiscount() {
		return $this->percentDiscount;
	}
	
	/**
	 * Set documentedAt
	 *
	 * @param \DateTime $documentedAt        	
	 * @return Invoice
	 */
	public function setDocumentedAt($documentedAt) {
		$this->documentedAt = $documentedAt;
		
		return $this;
	}
	
	/**
	 * Get documentedAt
	 *
	 * @return \DateTime
	 */
	public function getDocumentedAt() {
		return $this->documentedAt;
	}
	
	/**
	 * Set mustPayAt
	 *
	 * @param \DateTime $mustPayAt        	
	 * @return Invoice
	 */
	public function setMustPayAt($mustPayAt) {
		$this->mustPayAt = $mustPayAt;
		
		return $this;
	}
	
	/**
	 * Get mustPayAt
	 *
	 * @return \DateTime
	 */
	public function getMustPayAt() {
		return $this->mustPayAt;
	}
	
	/**
	 * Set soldAt
	 *
	 * @param \DateTime $soldAt        	
	 * @return Invoice
	 */
	public function setSoldAt($soldAt) {
		$this->soldAt = $soldAt;
		
		return $this;
	}
	
	/**
	 * Get soldAt
	 *
	 * @return \DateTime
	 */
	public function getSoldAt() {
		return $this->soldAt;
	}
	
	/**
	 * Set madeAt
	 *
	 * @param \DateTime $madeAt        	
	 * @return Invoice
	 */
	public function setMadeAt($madeAt) {
		$this->madeAt = $madeAt;
		
		return $this;
	}
	
	/**
	 * Get madeAt
	 *
	 * @return \DateTime
	 */
	public function getMadeAt() {
		return $this->madeAt;
	}
	
	/**
	 * Set offer
	 *
	 * @param string $offer        	
	 * @return Invoice
	 */
	public function setOffer($offer) {
		$this->offer = $offer;
		
		return $this;
	}
	
	/**
	 * Get offer
	 *
	 * @return string
	 */
	public function getOffer() {
		return $this->offer;
	}
	
	/**
	 * Set billFull
	 *
	 * @param boolean $billFull        	
	 * @return Invoice
	 */
	public function setBillFull($billFull) {
		$this->billFull = $billFull;
		
		return $this;
	}
	
	/**
	 * Get billFull
	 *
	 * @return boolean
	 */
	public function getBillFull() {
		return $this->billFull;
	}
	
	/**
	 * Set indicatorCompany
	 *
	 * @param boolean $indicatorCompany        	
	 * @return Invoice
	 */
	public function setIndicatorCompany($indicatorCompany) {
		$this->indicatorCompany = $indicatorCompany;
		
		return $this;
	}
	
	/**
	 * Get indicatorCompany
	 *
	 * @return boolean
	 */
	public function getIndicatorCompany() {
		return $this->indicatorCompany;
	}
	
	/**
	 * Set client
	 *
	 * @param \AppBundle\Entity\Client $client        	
	 * @return Invoice
	 */
	public function setClient(\AppBundle\Entity\Client $client = null) {
		$this->client = $client;
		
		return $this;
	}
	
	/**
	 * Get client
	 *
	 * @return \AppBundle\Entity\Client
	 */
	public function getClient() {
		return $this->client;
	}

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Invoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Invoice
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Invoice
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Invoice
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
