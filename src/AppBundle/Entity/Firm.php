<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Firm
 *
 * @ORM\Table(name="firm", options={"collate"="utf8_polish_ci"})
 * @ORM\Entity
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FirmRepository") 
 */
class Firm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="string", length=4, nullable=true)
     */
    private $year;

    /**
     * @var integer
     *
     * @ORM\Column(name="day_to_paid_default", type="smallint", nullable=true)
     */
    private $dayToPaidDefault;

    /**
     * @var float
     *
     * @ORM\Column(name="max_credit", type="float", precision=10, scale=0, nullable=true)
     */
    private $maxCredit;

    /**
     * @var float
     *
     * @ORM\Column(name="min_balance", type="float", precision=10, scale=0, nullable=true)
     */
    private $minBalance;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=150, nullable=true)
     */
    private $companyName;
    
    /**
     *
     * @var string @ORM\Column(name="krs", type="string", length=50, nullable=true)
     */
    private $krs;    

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=5, nullable=true)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_id", type="string", length=13, nullable=true)
     */
    private $vatId;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=25, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=25, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="post_office", type="string", length=25, nullable=true)
     */
    private $postOffice;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=25, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=50, nullable=true)
     */
    private $website;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;
    
    /**
     * @var \AppBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param string $year
     * @return Firm
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set dayToPaidDefault
     *
     * @param integer $dayToPaidDefault
     * @return Firm
     */
    public function setDayToPaidDefault($dayToPaidDefault)
    {
        $this->dayToPaidDefault = $dayToPaidDefault;

        return $this;
    }

    /**
     * Get dayToPaidDefault
     *
     * @return integer 
     */
    public function getDayToPaidDefault()
    {
        return $this->dayToPaidDefault;
    }

    /**
     * Set maxCredit
     *
     * @param float $maxCredit
     * @return Firm
     */
    public function setMaxCredit($maxCredit)
    {
        $this->maxCredit = $maxCredit;

        return $this;
    }

    /**
     * Get maxCredit
     *
     * @return float 
     */
    public function getMaxCredit()
    {
        return $this->maxCredit;
    }

    /**
     * Set minBalance
     *
     * @param float $minBalance
     * @return Firm
     */
    public function setMinBalance($minBalance)
    {
        $this->minBalance = $minBalance;

        return $this;
    }

    /**
     * Get minBalance
     *
     * @return float 
     */
    public function getMinBalance()
    {
        return $this->minBalance;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Firm
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return Firm
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set vatId
     *
     * @param string $vatId
     * @return Firm
     */
    public function setVatId($vatId)
    {
        $this->vatId = $vatId;

        return $this;
    }

    /**
     * Get vatId
     *
     * @return string 
     */
    public function getVatId()
    {
        return $this->vatId;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Firm
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Firm
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Get adres
     *
     * @return string
     */
    public function getAddressFull()
    {
    	$address = '';
    	if (strlen($this->getZipCode()) != 0) {
    		$address .= substr($this->getZipCode(), 0, 2) . '-' . substr($this->getZipCode(), 2, 3) . ' ';
    	}
    	$address .= trim($this->getPostOffice());
    	if (trim($this->getPostOffice()) != trim($this->getCity())) {
    		$address .= ", " . trim($this->getCity());
    	}
    	if (strlen($this->getAddress()) != 0) {
    		if (stripos('0123456789', substr($this->getAddress(), 0, 1))) {
    			$address .= ' ';
    		} else {
    			$address .= ', ';
    		}
    	}
    	$address .= trim($this->getAddress());
    
    	return $address;
    }

    /**
     * Set postOffice
     *
     * @param string $postOffice
     * @return Firm
     */
    public function setPostOffice($postOffice)
    {
        $this->postOffice = $postOffice;

        return $this;
    }

    /**
     * Get postOffice
     *
     * @return string 
     */
    public function getPostOffice()
    {
        return $this->postOffice;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Firm
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Firm
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Firm
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Firm
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set krs
     *
     * @param string $krs
     * @return Firm
     */
    public function setKrs($krs)
    {
        $this->krs = $krs;

        return $this;
    }

    /**
     * Get krs
     *
     * @return string 
     */
    public function getKrs()
    {
        return $this->krs;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Firm
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Firm
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Firm
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Firm
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
